package pc.info.solution.washdude;

import android.content.Intent;
import android.content.SharedPreferences;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import pc.info.solution.washdude.FragmentsPkG.OrderFragmnet;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.UTILLS.InputValidation;
import pc.info.solution.washdude.UTILLS.TypefaceSpan;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

public class CreateAccount extends AppCompatActivity {

    private EditText name, email, password, phone ,refby;
    FloatingActionButton fb;
    FloatingActionButton  gmailSignInButton;


    LoginButton loginButton;
    private String Name, Email, Mobile, Password, userId,Refby;
    private static final String TAG = "CreateAccount";
    private String gcmId = "";
    private GoogleCloudMessaging gcm;
    SharedPreferenceClass sharedPreferenceClass;
    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;
    private int mobileVerified = 0;

    private static final int INPUT_MOBILE_NUMBER = 1;
    private GoogleSignInResult result;
    public static CallbackManager callbackmanager;
    private AccessTokenTracker accessTokenTracker;
    private String accessToken;
    private Boolean isFacebookSignIn = false;
    private JSONObject facebookResult;
    RelativeLayout arrow;


    //    SharedPreferences shp ;
    RelativeLayout signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_create_account);
        gmailSignInButton = (FloatingActionButton) findViewById(R.id.gmail_fab1);
        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());


        fb = (FloatingActionButton) findViewById(R.id.fb1);
        name = (EditText) findViewById(R.id.fullname_edit);
        name.setHint(SpanString("Enter your name"));
        email = (EditText) findViewById(R.id.email_edit);
        email.setHint(SpanString("Enter your email"));
        password = (EditText) findViewById(R.id.ic_lock);
        password.setHint(SpanString("Enter your password"));
        phone = (EditText) findViewById(R.id.ic_phone);
        phone.setHint(SpanString("Enter your phone number"));
        refby = (EditText) findViewById(R.id.ic_refby);
        refby.setHint(SpanString("Enter your refer"));
        arrow = (RelativeLayout) findViewById(R.id.backbtn);

        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Intent i = new Intent(CreateAccount.this,Pre_LOGIN.class);
               startActivity(i);


                finish();
            }
        });

        signup = (RelativeLayout) findViewById(R.id.signup);

        // GCM
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
            Log.d(TAG, "Play Services Ok.");
            if (gcmId.isEmpty()) {
                Log.d(TAG, "Find Register ID.");
                registerInBackground();
            }

        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }

////////General Signup//////////
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                				/*if (!InputValidation.isEditTextHasvalue(name)) {
                    name.setError("Enter your name");
				} else*/
                if (!InputValidation.isEditTextHasvalue(email)) {
                    email.setError("Enter email id");
                } else if (!InputValidation.isEditTextContainEmail(email)) {
                    email.setError("Enter a valid email id");
                } else if (!InputValidation.isEditTextHasvalue(phone)) {
                    phone.setError("Enter your mobile number");
                } /*else if (!InputValidation.isNumberselected(phone)) {
                    phone.setError("Enter a valid mobile number");
                } else if (!InputValidation.isEditTextHasvalue(password)) {
                    password.setError("Enter password");
                } else if (!InputValidation.isPasswordLengthCheck(password)) {
                    password.setError("Enter a valid password");
                } */ else {
                    Name = name.getText().toString();
                    Email = email.getText().toString();
                    Mobile = phone.getText().toString();
                    Password = password.getText().toString();
                    Refby = refby.getText().toString();
                    signUpTask(Serverlinks.signUp);
                }
            }


        });


////////Fb method///////////////

        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFacebookSignIn = true;
                LoginManager.getInstance().logInWithReadPermissions(CreateAccount.this, Arrays.asList("email"));
            }
        });


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(androidClientID)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        gmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isFacebookSignIn = false;
                googleSignIn();

            }
        });
////////Fb method///////////////

        getFacebookUserInfo();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
        if (requestCode == RC_SIGN_IN) {
            result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleGoogleSignInResult(result, null);
        } else if (requestCode == INPUT_MOBILE_NUMBER) {
            String mobileNumber = data.getStringExtra(Constants.mobile);
            if (isFacebookSignIn)
                handleFacebookSignInResult(facebookResult, mobileNumber);
            else
                handleGoogleSignInResult(result, mobileNumber);
        } else {

            callbackmanager.onActivityResult(requestCode, resultCode, data);

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(CreateAccount.this,Pre_LOGIN.class);
        startActivity(i);

        finish();
    }

    //After the signing we are calling this function
    private void handleGoogleSignInResult(GoogleSignInResult result, String mobile) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("name", acct.getDisplayName());
                jsonObject.accumulate("email", acct.getEmail());
                jsonObject.accumulate("google_id", acct.getId());
                jsonObject.accumulate("gcm_id", gcmId);
                jsonObject.accumulate("login_type", 2);
                jsonObject.accumulate("mobile", mobile);
                Log.d(TAG, jsonObject.toString());
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Serverlinks.signUp, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e(TAG, "Signup Response: " + response.toString());

                            if (response != null) {
                                handleJsonResponse(response);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Error: " + error.getMessage());
                    }
                });
                MyApplication.getInstance().addToRequestQueue(jsonObjectRequest);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }

    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    private String signUpTask(String url) {


        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("name", Name);
            jsonObject.accumulate("email", Email);
            jsonObject.accumulate("mobile", Mobile);
            jsonObject.accumulate("password", Password);
            jsonObject.accumulate("gcm_id", gcmId);
            jsonObject.accumulate("ref_by", Refby);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {


                    try {
                        Log.e(TAG, "Signup Response: " + response.toString());

                        if (response != null) {
                            handleJsonResponse(response);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Error: " + error.getMessage());
                }
            });
            MyApplication.getInstance().addToRequestQueue(jsonObjectRequest);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void handleJsonResponse(JSONObject response) {
        try {
            int status = response.getInt("status");
            if (status == 0) {

            } else if (status == 1) {

                JSONObject entry = response.getJSONObject("data");
                Name = entry.getString(Constants.name);
                userId = entry.getString(Constants.userId);
                Email = entry.getString(Constants.email);
                Mobile = entry.getString(Constants.mobile);
                Refby = entry.getString(Constants.ref_by);
                mobileVerified = entry.getInt(Constants.mobileVerified);
                saveUserDetails();
                if (mobileVerified == 0) {
                    Intent intent = new Intent(CreateAccount.this, OtpConfirmation.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(CreateAccount.this, HOMEPAGE.class);
                    startActivity(intent);
                    finish();
                }
            } else if (status == 400) {
                Log.d(TAG, "Asking for mobile number");
                showMobileNumberActivity();
            } else {
                String data = response.getString("data");
                Toast.makeText(getBaseContext(), data, Toast.LENGTH_SHORT).show();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showMobileNumberActivity() {
        Intent i = new Intent(CreateAccount.this, MobileNumberActivity.class);
        startActivityForResult(i, INPUT_MOBILE_NUMBER);


    }

    private void saveUserDetails() {
        try {


            sharedPreferenceClass.setValue_string(Constants.userId, userId);
            sharedPreferenceClass.setValue_string(Constants.name, Name);
            sharedPreferenceClass.setValue_string(Constants.mobile, Mobile);
            sharedPreferenceClass.setValue_int(Constants.mobileVerified, mobileVerified);
            sharedPreferenceClass.setValue_boolean(Constants.isUserLoggedIn, true);
            sharedPreferenceClass.setValue_string(Constants.email, Email);
            sharedPreferenceClass.setValue_string(Constants.Who_Login, "USER");

        } catch (Exception e) {
            e.printStackTrace();

            finish();

        }
    }


    private Bundle getFacebookData(JSONObject object) {

        Bundle bundle = new Bundle();
        try {

            String id = object.getString("id");

            URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=250&height=250");
            bundle.putString("profile_pic", profile_pic.toString());
            bundle.putString("idFacebook", id);

            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            /*if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));*/


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        return bundle;
    }


    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }


    public SpannableString SpanString(String str) {

        SpannableString s1 = new SpannableString(str);
        s1.setSpan(new TypefaceSpan(this, "urban.otf"), 0, s1.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return s1;

    }


    private void getFacebookUserInfo() {

        Log.d(TAG, "Inside getFacebookUserInfo");

        callbackmanager = CallbackManager.Factory.create();

        // Set permissions
        // LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_photos", "public_profile"));
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
                if (currentAccessToken != null)
                    accessToken = currentAccessToken.getToken();
            }
        };
        // If the access token is available already assign it.
//		accessToken = AccessToken.getCurrentAccessToken().getToken();

        LoginManager.getInstance().registerCallback(callbackmanager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {

                        System.out.println("Success" + loginResult.getAccessToken().getToken());
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject json, GraphResponse response) {
                                        if (response.getError() != null) {
                                            // handle error
                                            Log.d(TAG, "Facebook Login Error");
                                        } else {
                                            Log.d(TAG, "Facebook Login Success");
                                            try {

//                                                loginRequestVO =new LoginRequestVO();
                                                String jsonresult = String.valueOf(json);
                                                System.out.println("JSON Result" + jsonresult);
                                                facebookResult = json;
                                                handleFacebookSignInResult(json, null);


//                                                new AuthLoginAsyncTask().execute(ServerLinks.authLogin);

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, name, email"); // Parámetros que pedimos a facebook
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        //Log.d(TAG_CANCEL,"On cancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(CreateAccount.this, error.toString(), Toast.LENGTH_SHORT).show();
                        //Log.d(TAG_ERROR,error.toString());
                    }
                });


    }


    //After the signing we are calling this function
    private void handleFacebookSignInResult(JSONObject result, String mobile) {
        //If the login succeed

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("name", result.getString("name"));
            jsonObject.accumulate("email", result.getString("email"));
            jsonObject.accumulate("fb_id", result.getString("id"));
            jsonObject.accumulate("gcm_id", gcmId);
            jsonObject.accumulate("login_type", 1);
            jsonObject.accumulate("mobile", mobile);
            Log.d(TAG, "Facebook Request : " + jsonObject.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Serverlinks.signUp, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        Log.e(TAG, "Facebook Signin Response: " + response);

                        if (response != null) {
                            handleJsonResponse(response);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Error: " + error.getMessage());
                }
            });
            MyApplication.getInstance().addToRequestQueue(jsonObjectRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, 9000).show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(CreateAccount.this);
                    }
                    gcmId = gcm.register(Constants.googleGCMApiSenderId);
                    msg = "Device registered, registration ID=" + gcmId;
                    Log.d(TAG, "ID GCM: " + gcmId);
                    setGcmId(gcmId);

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);


    }


    public void setGcmId(String gcmId) {
        Log.e("gcmId ", "" + gcmId);
        sharedPreferenceClass.setValue_string(Constants.gcmId, gcmId);
    }
}
