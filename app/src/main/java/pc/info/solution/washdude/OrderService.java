package pc.info.solution.washdude;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pc.info.solution.washdude.AdapterPkg.TabPageAdapter;
import pc.info.solution.washdude.FragmentsPkG.OrderFragmnet;
import pc.info.solution.washdude.UTILLS.PrefManager;
import pc.info.solution.washdude.UTILLS.TypefaceSpan;
import pc.info.solution.washdude.webservices.Serverlinks;

public class OrderService extends AppCompatActivity {

    static TabLayout tabLayout;
    static ViewPager viewPager;
    static TabPageAdapter pageAdapter;
    public static TextView finalPricetext;
    CoordinatorLayout coordinatorLayout;
    RelativeLayout next;

    String userID = "userID";
    PrefManager shp;
    public FragmentManager manager = getSupportFragmentManager();

    static ProgressDialog progressDialog;

    static ArrayList<String> parentnameList, parentIconlist;

    /*
        static int TabIcons[]={R.drawable.tab_man,
                R.drawable.tab_woman,R.drawable.tab_household,R.drawable.tab_child,R.drawable.tab_accessory};*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_service);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(this);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.cordinate);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString("ORDERS");
        s.setSpan(new TypefaceSpan(this, "AvenirLTStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(s);
        shp = new PrefManager(this, userID);

        finalPricetext = (TextView) findViewById(R.id.finalPricetext);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        next = (RelativeLayout) findViewById(R.id.total_layout);

        pageAdapter = new TabPageAdapter(getSupportFragmentManager());

        float p = shp.GetFinalPrice();
        setFinalPriceIntext(p);


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (finalPricetext.getText().toString().equals("")){
                    Snackbar snackbar = Snackbar.
                            make(coordinatorLayout,"PLEASE MAKE ORDER",Snackbar.LENGTH_LONG);
                    snackbar.show();
                }else {
                    Intent intent = new Intent(OrderService.this, ChooseSlot.class);
                    startActivity(intent);
                    finish();

                }


            }
        });

        DownloadOrderList(Serverlinks.clothlist, getBaseContext(),0);


    }

    public static void setFinalPriceIntext(Float p) {

        if (p > 0) {

            OrderService.finalPricetext.setText("Rs " + String.valueOf(p) + "0");

        } else {
            OrderService.finalPricetext.setText("");

        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 9 && resultCode == Activity.RESULT_OK) {


            recreate();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
        }


        return super.onOptionsItemSelected(item);
    }


    public static void DownloadOrderList(String url, final Context context,final int i) {

        progressDialog.setMessage("Fetching Cloth Details");
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    Log.e("ORDER SERVICE", "List of photos json reponse: " + response.toString());

                    if (response != null) {
                        parseJsonFeed(response, context,i);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                Log.e("ORDER SERVICE", "Error: " + error.getMessage());
            }
        });


        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest);


    }

    private static void parseJsonFeed(JSONObject response, Context context,int number) {


        parentIconlist = new ArrayList<>();
        parentnameList = new ArrayList<>();

        pageAdapter.clearFrag();
        try {
            int status = response.getInt("status");
            if (status == 0) {

            } else if (status == 1) {

                JSONArray entry = response.getJSONArray("data");
                for (int i = 0; i < entry.length(); i++) {
                    JSONObject json1 = entry.getJSONObject(i);
                    String parentName = json1.getString("Parent");
                    parentnameList.add(parentName);

                    String parentId = json1.getString("parent_id");
                    String parent_icon_url = json1.getString("img");
                    parentIconlist.add(parent_icon_url);

                    Fragment fragment = OrderFragmnet.newInstance(json1, parent_icon_url,i);
                    pageAdapter.addFrag(fragment, parentName);
                }

                viewPager.setAdapter(pageAdapter);
                tabLayout.setupWithViewPager(viewPager);
                viewPager.setCurrentItem(number);

                for (int i = 0; i < tabLayout.getTabCount(); i++) {

                    View tabOne = LayoutInflater.from(context).inflate(R.layout.tab_text, null);
                    TextView text = (TextView) tabOne.findViewById(R.id.tab);
                    ImageView img = (ImageView) tabOne.findViewById(R.id.img_ab);
                    Glide.with(context).load(parentIconlist.get(i)).thumbnail(0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).into(img);

                    text.setText(parentnameList.get(i));

                    tabLayout.getTabAt(i).setCustomView(tabOne);


                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}
