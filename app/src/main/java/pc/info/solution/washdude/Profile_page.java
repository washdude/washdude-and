package pc.info.solution.washdude;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pc.info.solution.washdude.FragmentsPkG.Adress_frag;
import pc.info.solution.washdude.FragmentsPkG.PassWord_frag;
import pc.info.solution.washdude.FragmentsPkG.Profile_fragment;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.UTILLS.ProfileAddress;
import pc.info.solution.washdude.webservices.AllStaticVariables;
import pc.info.solution.washdude.webservices.AsynkTaskCommunicationInterface;
import pc.info.solution.washdude.webservices.AsynkTaskForServerCommunication;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

public class Profile_page extends AppCompatActivity implements AsynkTaskCommunicationInterface {

    RelativeLayout profile_check,password_check,address_check,middile_ ;
    ImageView back_img;
    CircleImageView img;
    RelativeLayout blur;
    CollapsingToolbarLayout collaps;
    TextView name , phone , title_select,email ;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    private SharedPreferenceClass sharedPreferenceClass;
    private String userId;
    String json_username, json_name, json_mob, json_email, json_refcode,json_img;
    List<ProfileAddress> profileAddresses;
    String input_name,input_mob,input_email;
    ImageView pick_image;
    File image = null;
    SharedPreferences.Editor editor;
    SharedPreferences preference;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        collaps = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        collaps.setTitle("");
        preference = getSharedPreferences("PROFILE_PIC", MODE_APPEND);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        profile_check = (RelativeLayout) findViewById(R.id.id_profile);
        password_check = (RelativeLayout) findViewById(R.id.id_pass);
        address_check = (RelativeLayout) findViewById(R.id.id_adress);
        middile_ = (RelativeLayout) findViewById(R.id.middle_divider);
        img = (CircleImageView) findViewById(R.id.circle_profile_pic);
        name = (TextView) findViewById(R.id.name_profile);
        phone = (TextView) findViewById(R.id.phone_no);
        email = (TextView) findViewById(R.id.email_id);
        title_select = (TextView) findViewById(R.id.title_select);
        pick_image = (ImageView) findViewById(R.id.pick_image);
        back_img = (ImageView) findViewById(R.id.back_img);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);


        pick_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        editor = preference.edit();

        String input = preference.getString("imagePreferance", null);

        if (input == null) {

            try {

            } catch (Exception e) {

                e.printStackTrace();
            }

        } else {
            Log.e("encoded_bitmap", input);
            Bitmap profile_bit = decodeBase64(input);
            img.setImageBitmap(profile_bit);
        }

        profile_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input_name = name.getText().toString();
                input_mob = phone.getText().toString();
                input_email = email.getText().toString();

                profile_check.setBackgroundColor(getResources().getColor(R.color.light_green_but_dark));
                password_check.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                address_check.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                middile_.setVisibility(View.VISIBLE);
                Fragment fragment = new Profile_fragment();
                Bundle bundle = new Bundle();
                bundle.putString("name",input_name);
                bundle.putString("mob",input_mob);
                bundle.putString("email",input_email);
                fragment.setArguments(bundle);
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.conatiner,fragment);
                transaction.commit();
                title_select.setText("PROFILE");

            }
        });

        password_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password_check.setBackgroundColor(getResources().getColor(R.color.light_green_but_dark));
                profile_check.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                address_check.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                middile_.setVisibility(View.VISIBLE);
                title_select.setText("PASSWORD");
                Fragment fragment = new PassWord_frag();
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.conatiner,fragment);
                transaction.commit();
            }
        });

        address_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkAddress();
            }
        });

        refresService();

        checkReqPermissions();

    }

    private void checkAddress(){
        password_check.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        profile_check.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        address_check.setBackgroundColor(getResources().getColor(R.color.light_green_but_dark));
        middile_.setVisibility(View.VISIBLE);
        title_select.setText("ADDRESS");
        Fragment fragment = new Adress_frag();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.conatiner,fragment);
        transaction.commit();

    }
    private void selectImage() {
        final CharSequence[] options = { "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(Profile_page.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                /*if (options[item].equals("Take Photo")) {


                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imgFilename = "Wd_profile" + timeStamp;

                    File nfile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);


                     image = new File(nfile,imgFilename);

                  *//*  try {
                        image = File.createTempFile(
                                imgFilename,  *//*//**//* prefix *//**//**//**//*
                                ".jpg",         *//*//**//* suffix *//**//**//**//*
                                nfile      *//*//**//* directory *//**//**//**//*
                        );

                    } catch (IOException e) {
                        e.printStackTrace();
                    }*//*



                      Uri curi =Uri.fromFile(image);

                    editor.clear();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,curi);
                    startActivityForResult(intent, 3);
                }*/  if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent, 2);
                    editor.clear();

                } else if (options[item].equals("Cancel")) {
                    //dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 31) {
            if (resultCode == RESULT_OK) {




            }

        } else if (requestCode == 2 && resultCode == RESULT_OK) {

            Uri uri = data.getData();

            // choose from gallery
            convert_bitmap(uri);

        }
    }

    public void convert_bitmap(Uri uri) {

        Bitmap bitmap = null;


        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
        } catch (Exception e) {
            e.printStackTrace();
        }

        img.setImageBitmap(bitmap);

        save_in_preferrence(bitmap);

    }


    public void convert_image_bitmap(File image) {


        Bitmap  bitmap = null;


        try {

             bitmap = BitmapFactory.decodeFile(image.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }

        img.setImageBitmap(bitmap);

        save_in_preferrence(bitmap);
    }

    private void save_in_preferrence(Bitmap bitmap) {


        editor.putString("imagePreferance", encodeTobase64(bitmap));
        editor.commit();

    }

    private String encodeTobase64(Bitmap bitmap) {


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.d("Image Log:", imageEncoded);
        return imageEncoded;

    }
    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
    @Override
    protected void onRestart() {
        super.onRestart();

    }


    private static final int READ_CONTACTS_PERMISSIONS_REQUEST = 1234;

    public void checkReqPermissions(){
        if (!hasPermissions(getBaseContext(),PERMISSIONS)){

            ActivityCompat.requestPermissions(this, PERMISSIONS, READ_CONTACTS_PERMISSIONS_REQUEST);

        }

    }



    public static boolean hasPermissions(Context context , String... permissions){


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context!=null && permissions != null){

            for (String permission : permissions){

                if (ActivityCompat.checkSelfPermission(context,permission)!= PackageManager.PERMISSION_GRANTED){

                    return false ;
                }
            }
        }

        return true ;


    }



    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }





    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (requestCode == READ_CONTACTS_PERMISSIONS_REQUEST  ) {

            if (grantResults.length == 3 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[2] == PackageManager.PERMISSION_GRANTED)

            {





            }else {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE)
                        || ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        || ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA))
                {


                    showDialogOK("STORAGE Permission required for this app",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkReqPermissions();
                                            break;
                                        case DialogInterface.BUTTON_NEGATIVE:
                                            finish();
                                            // proceed with logic by disabling the related features or quit the app.
                                            break;
                                    }
                                }
                            });
                } else {
                    Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG)
                            .show();

                }
            }


        }

    }





    private void refresService() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("user_id", userId);
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, Serverlinks.user_profile);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(Profile_page.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);
    }
    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }

    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if (jsonObject != null) {
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status = jsonObject.getInt("status");
                String data = jsonObject.getString("data");
                //Create a jsonObject of String data
                JSONObject dataObject = new JSONObject(data);
                if (status == 0) {
                    Toast.makeText(Profile_page.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                } else if (status == 1) {

                    json_username = dataObject.getString("username");
                    json_name = dataObject.getString("name");
                    json_mob = dataObject.getString("mobile");
                    json_email = dataObject.getString("email");
                    json_refcode = dataObject.getString("ref_code");
                    json_img = dataObject.getString("back_image");

                    name.setText(json_name);
                    phone.setText(json_mob);
                    email.setText(json_email);

                    Uri myUri = Uri.parse(json_img);
                    Glide.with(this)
                            .load(myUri)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .crossFade()
                            .thumbnail(0.5f)
                            .into(back_img);
                    final JSONArray jArr = dataObject.getJSONArray("address");
                    profileAddresses = new Gson().fromJson(jArr.toString(), new TypeToken<ArrayList<ProfileAddress>>() {
                    }.getType());
                    /*if (profileAddresses == null)
                        profileAddresses = new ArrayList<ProfileAddress>();
                    txtAdr.setVisibility(View.VISIBLE);
                    txtAdr.setText("Please add address for order");
                    if(!(profileAddresses.size()==0)){
                        txtAdr.setVisibility(View.INVISIBLE);
                    }
                    Address_recycler_adapter address_recycler_adapter = new Address_recycler_adapter(ProfilePage.this, profileAddresses);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                    layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    layoutManager.scrollToPosition(0);
                    adressListview.setLayoutManager(layoutManager);
                    adressListview.setAdapter(address_recycler_adapter);*/// set adapter


                }
                checkAddress();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}


