package pc.info.solution.washdude;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import pc.info.solution.washdude.AdapterPkg.Order_list_adapter;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

public class OrderListActivity extends AppCompatActivity {

    RecyclerView oredrListView;
    Order_list_adapter adapter;
    private SharedPreferenceClass sharedPreferenceClass;
    private String userId;
    String[] json_orderId, json_address, json_orderdt,json_img;
    int[] json_orderstatus;
    TextView txtOrder;
    ProgressDialog progressDialog;
    ProgressBar progressBar;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("ORDER LIST");

        sharedPreferenceClass = new SharedPreferenceClass(getBaseContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);

        progressBar = (ProgressBar) findViewById(R.id.offer_new_prog);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLay);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        oredrListView = (RecyclerView) findViewById(R.id.track_order_list);
        txtOrder = (TextView) findViewById(R.id.txtOrder);


        new orderhistory_listAsyntask().execute(Serverlinks.order_data);
    }


    private class orderhistory_listAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", userId);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);

            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {

                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject != null) {
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            JSONArray jArr = jsonObject.getJSONArray("data");
                            if (jArr != null) {

                                json_orderId = new String[jArr.length()];
                                json_address = new String[jArr.length()];
                                json_orderdt = new String[jArr.length()];
                                json_img = new String[jArr.length()];
                                json_orderstatus = new int[jArr.length()];
                                //orderImage = new int[jArr.length()];
                                for (int i = 0; i < jArr.length(); i++) {
                                    jsonResponse = jArr.getJSONObject(i);
                                    json_orderId[i] = jsonResponse.getString("order_id");
                                    json_address[i] = jsonResponse.getString("address");
                                    json_orderstatus[i] = jsonResponse.getInt("orderstatus");
                                    json_img[i] = jsonResponse.getString("status_image");
                                    json_orderdt[i] = jsonResponse.getString("order_date");
                                }
                            }
                            adapter = new Order_list_adapter(getBaseContext(), json_orderId, json_orderdt, json_orderstatus,json_img);
                            oredrListView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                            oredrListView.setAdapter(adapter);

                            // set adapter

                        }
                        if (status == 2) {

                            txtOrder.setVisibility(View.VISIBLE);
                            txtOrder.setText("Order Not Placed Yet");

                        }
                    } else {
                        Snackbar snackbar = Snackbar.
                                make(coordinatorLayout,"Please select Pickup ,Delivery Date and an Adress before proceeding",Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                } else {
                    Snackbar snackbar = Snackbar.
                            make(coordinatorLayout,"Please select Pickup ,Delivery Date and an Adress before proceeding",Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            } catch (Exception e) {
                Log.e("String ", e.toString());
            }
            progressBar.setVisibility(View.GONE);
            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

}
