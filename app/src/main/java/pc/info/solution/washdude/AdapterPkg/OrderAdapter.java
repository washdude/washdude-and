package pc.info.solution.washdude.AdapterPkg;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import pc.info.solution.washdude.R;
import pc.info.solution.washdude.UTILLS.PrefManager;
/**
 * Created by abhis on 7/19/2016.
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyHolder> {

    Context context;
    ArrayList<String> clothnames, clothurls, cloth_ids;
    String userID;
    PrefManager pref;
    int count;


    public OrderAdapter(Context baseContext, ArrayList<String> clothnames, ArrayList<String> clothurls, String userID, ArrayList<String> cloth_ids) {
        this.context = baseContext;
        this.clothnames = clothnames;
        this.clothurls = clothurls;
        this.userID = userID;
        this.cloth_ids = cloth_ids;
        pref = new PrefManager(context, userID);

    }

    @Override
    public OrderAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.each_order_item, parent, false);
        MyHolder holder = new MyHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(OrderAdapter.MyHolder holder, int position) {


        count = pref.GetPrimaryCount(cloth_ids.get(position));


        if (count == 0) {
            holder.primary_count_lay.setVisibility(View.GONE);

        } else {
            holder.primary_count_lay.setVisibility(View.VISIBLE);
            holder.count.setText("x" + String.valueOf(count));
        }
        holder.blur.getBackground().setAlpha(120);
        holder.name.setText(clothnames.get(position));
        Glide.with(context).load(clothurls.get(position))
                .thumbnail(0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.img);

    }

    @Override
    public int getItemCount() {
        return clothnames.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView name, count;
        RelativeLayout blur, primary_count_lay;


        public MyHolder(View itemView) {
            super(itemView);

            img = (ImageView) itemView.findViewById(R.id.thumbnail);
            name = (TextView) itemView.findViewById(R.id.cloth_name);
            blur = (RelativeLayout) itemView.findViewById(R.id.blur);
            count = (TextView) itemView.findViewById(R.id.primary_count);
            primary_count_lay = (RelativeLayout) itemView.findViewById(R.id.primary_count_lay);
        }
    }
}
