package pc.info.solution.washdude.FragmentsPkG;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import de.hdodenhof.circleimageview.CircleImageView;
import pc.info.solution.washdude.R;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;


/**
 * Created by Abhishek on 6/7/2016.
 */
public class FragmentDrawer extends Fragment implements View.OnClickListener {

    RelativeLayout lay1,lay2,lay3,lay4,lay5,lay6,lay7,lay8,back_lay ;
    public static TextView text_name;
    public static TextView text_mob;
    private SharedPreferences sharedPreferences;
    String userName,userId;
    String userMob;

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View containerView;
    RelativeLayout background;
    private FragmentDrawerListener drawerListener;
    private SharedPreferenceClass sharedPreferenceClass;


    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        sharedPreferenceClass = new SharedPreferenceClass(getContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);
        userName = sharedPreferenceClass.getValue_string(Constants.name);
        userMob = sharedPreferenceClass.getValue_string(Constants.mobile);


        lay1 = (RelativeLayout) view.findViewById(R.id.lay1);
        lay2 = (RelativeLayout) view.findViewById(R.id.lay2);
        lay3 = (RelativeLayout) view.findViewById(R.id.lay3);
        lay4 = (RelativeLayout) view.findViewById(R.id.lay4);
        lay5 = (RelativeLayout) view.findViewById(R.id.lay5);
        lay6 = (RelativeLayout) view.findViewById(R.id.lay6);
        lay7 = (RelativeLayout) view.findViewById(R.id.lay7);
        lay8 = (RelativeLayout) view.findViewById(R.id.lay8);
        back_lay = (RelativeLayout) view.findViewById(R.id.back_lay);
        text_name = (TextView) view.findViewById(R.id.name_text);
        text_mob = (TextView) view.findViewById(R.id.mob_text);

        text_name.setText(userName);
        text_mob.setText("Mob : +91-"+userMob);



        back_lay.getBackground().setAlpha(100);


        background = (RelativeLayout) view.findViewById(R.id.fragment_navigation_drawer);


        lay1.setOnClickListener(this);
        lay2.setOnClickListener(this);
        lay3.setOnClickListener(this);
        lay4.setOnClickListener(this);
        lay5.setOnClickListener(this);
        lay6.setOnClickListener(this);
        lay7.setOnClickListener(this);
        lay8.setOnClickListener(this);



        //new User_HttpAsyncTask().execute(Serverlinks.user_profile);



        return view;



    }


    private class User_HttpAsyncTask extends AsyncTask<String, Void, String> {
        //ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", userId);
                /*deviceid1
						deviceid2
				senderreferral*/
                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }
            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String result) {
            // progressBar.setVisibility(View.GONE);
            try {
                if (!result.equals("")) {
                    JSONObject jObject = new JSONObject(result);
                    String status = jObject.optString("status");
                    String data = jObject.getString("data");
                    //Create a jsonObject of String data
                    JSONObject dataObject = new JSONObject(data);
                    if (status.equals("1")) {
                        String json_username = dataObject.getString("name");
                        String json_mob = dataObject.getString("mobile");


                      text_name.setText(json_username);text_mob.setText(json_mob);
                    } else {
                        Toast.makeText(getContext(), "Failed", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Bad Network Connection !!",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            // progressDialog.dismiss();
            super.onPostExecute(result);
        }

    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }



    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {

        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();

            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }


    public void closeDrawer(){

        mDrawerLayout.closeDrawers();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.lay1 :
                drawerListener.onDrawerItemSelected(view,1);
                break;
            case R.id.lay2:
                drawerListener.onDrawerItemSelected(view,2);
                break;
            case R.id.lay3:
                drawerListener.onDrawerItemSelected(view,3);
                break;
            case R.id.lay4:
                drawerListener.onDrawerItemSelected(view,4);
                break;
            case R.id.lay5:
                drawerListener.onDrawerItemSelected(view,5);
                break;
            case R.id.lay6:
                drawerListener.onDrawerItemSelected(view,6);
                break;
            case R.id.lay7:
                drawerListener.onDrawerItemSelected(view,7);
                break;
            case R.id.lay8:
                drawerListener.onDrawerItemSelected(view,8);
                break;


        }

    }


    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }





}
