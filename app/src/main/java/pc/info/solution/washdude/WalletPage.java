package pc.info.solution.washdude;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import pc.info.solution.washdude.AdapterPkg.WalletAdapter;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.webservices.AllStaticVariables;
import pc.info.solution.washdude.webservices.AsynkTaskCommunicationInterface;
import pc.info.solution.washdude.webservices.AsynkTaskForServerCommunication;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;
public class WalletPage extends AppCompatActivity implements AsynkTaskCommunicationInterface {

    RecyclerView walletList;
    WalletAdapter adapter;
    private SharedPreferenceClass sharedPreferenceClass;
    private String userId;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    String wallet_amount;
    TextView txtCurBal,text_no_transc;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        txtCurBal = (TextView) findViewById(R.id.currBal);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinate);
        text_no_transc= (TextView) findViewById(R.id.no_transc_text);

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("user_id", userId);
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, Serverlinks.walletdtls);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(WalletPage.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);


        walletList = (RecyclerView) findViewById(R.id.walletList);
        walletList.setLayoutManager(new LinearLayoutManager(this));


    }

    //======
    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }

    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if (jsonObject != null) {
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status = jsonObject.getInt("staus");
                if (status == 0) {
                    Snackbar snackbar = Snackbar.
                            make(coordinatorLayout,"Insufficient Data ",Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (status == 1) {

                    text_no_transc.setVisibility(View.GONE);
                    wallet_amount = jsonObject.getString("walletamount");

                    if (wallet_amount.equals("0")){
                        text_no_transc.setVisibility(View.VISIBLE);
                    }
                    final JSONArray jArr = jsonObject.getJSONArray("data");
                    if (jArr != null) {

                        adapter = new WalletAdapter(getBaseContext(),jArr);
                        walletList.setAdapter(adapter);

                    }
                    txtCurBal.setText("Rs." + wallet_amount +"/-");
                }else {
                    text_no_transc.setVisibility(View.VISIBLE);

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }/*else {
            Intent i = new Intent(Wallet_statement.this, NointernetActivity.class);
            startActivity(i);
            Wallet_statement.this.overridePendingTransition(R.anim.bot_top, R.anim.bot_top);
            finish();
        }*/
    }


}
