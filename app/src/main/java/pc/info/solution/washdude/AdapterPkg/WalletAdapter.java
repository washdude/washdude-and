package pc.info.solution.washdude.AdapterPkg;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pc.info.solution.washdude.R;

/**
 * Created by abhis on 8/3/2016.
 */
public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.WallHolder> {

    Context context;
    JSONArray walletlist;

   public WalletAdapter(Context context, JSONArray walletlist){
       this.context = context;
       this.walletlist=walletlist;


   }

    @Override
    public WallHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.wallet_item,parent,false);
        WallHolder holder = new WallHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(WallHolder holder, int position) {
        try {
            JSONObject jobj = walletlist.getJSONObject(position);
            String type;
            String jsontype=jobj.getString("type");
            if(jsontype.equals("1")){

                type="Rs."+jobj.getString("amount");
                holder.icon.setImageResource(R.drawable.minus);
               // holder.walletstatus.setText("Dr");
            }else{

                type="Rs."+jobj.getString("amount");
                holder.icon.setImageResource(R.drawable.plus);
                //holder.walletstatus.setText("Cr");
            }
            String new_order;
            String order_id=jobj.getString("transaction_id");
            new_order="Txn Id "+order_id;
            holder.date_.setText(jobj.getString("add_date"));
            holder.desc.setText(jobj.getString("remark"));
            holder.id_.setText(new_order);
            holder.wall_price.setText(type);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return walletlist.length();
    }

    public class WallHolder extends RecyclerView.ViewHolder{

        TextView _name,id_,wall_price,date_,desc;
        ImageView icon;


        public WallHolder(View itemView) {
            super(itemView);

            icon = (ImageView) itemView.findViewById(R.id.wallet_ic);

            wall_price = (TextView) itemView.findViewById(R.id.price_);
            date_ = (TextView) itemView.findViewById(R.id.date_);
            id_ = (TextView) itemView.findViewById(R.id.id_);
            desc = (TextView) itemView.findViewById(R.id.desc);
        }
    }
}
