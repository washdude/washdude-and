package pc.info.solution.washdude;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import pc.info.solution.washdude.FragmentsPkG.Adress_frag;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.UTILLS.ProfileAddress;
import pc.info.solution.washdude.UTILLS.SpinnerData;
import pc.info.solution.washdude.UTILLS.TypefaceSpan;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

public class Add_address extends AppCompatActivity {
    Spinner citySpinner, areaSpinner;
    EditText tag, addresss, landmark,pincode;
    private SharedPreferences shp;
    private ProfileAddress addressObj;


    public static String newc_name, newc_id, newlocn_name, newlocn_id;

    ArrayList<SpinnerData> cityArray,areaArray;

    public final static int REQUEST_CODE = 1;
    FrameLayout progress;
    RelativeLayout submit;
    TextView txtSave;
    private SharedPreferences sharedPreferences;
    private SharedPreferenceClass sharedPreferenceClass;
    private String userId;
    CoordinatorLayout coordinatorLayout ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addressObj=(ProfileAddress)getIntent().getSerializableExtra("address");
        if(addressObj==null){
            addressObj=new ProfileAddress();
        }
        inIt();
        setContentView(R.layout.activity_add_address);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.corinate);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        SpannableString s = new SpannableString("MANAGE ADDRESS");
        s.setSpan(new TypefaceSpan(this, "AvenirLTStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        citySpinner = (Spinner) findViewById(R.id.city_spinner);
        areaSpinner = (Spinner) findViewById(R.id.area_spinner);

        tag = (EditText) findViewById(R.id.tag_edit);
        addresss = (EditText) findViewById(R.id.adress_edit);
        landmark = (EditText) findViewById(R.id.landmark_edit);
        pincode = (EditText) findViewById(R.id.pin_edit);
        txtSave = (TextView) findViewById(R.id.text_save);

        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);


        new c_cityAsyncTask().execute(Serverlinks.choose_city);

        submit = (RelativeLayout) findViewById(R.id.submitLayout);
        if(addressObj.getAddr_id()!=null){
            txtSave.setText("Update");
        }

        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                getValues();

                if (tag.getText().toString().equalsIgnoreCase("") || addresss.getText().toString().equalsIgnoreCase("")
                        || landmark.getText().toString().equalsIgnoreCase("") || pincode.getText().toString().equalsIgnoreCase("")) {

                    Snackbar snackbar = Snackbar.
                            make(coordinatorLayout,"Fill all the Above Fields",Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {



                    new submit_addAsyncTask().execute(Serverlinks.submit_address);
                    submit.setVisibility(View.GONE);

                }
            }
        });
		/*
		 * tag.setText(tagg); addresss.setText(addrs); landmark.setText(lndmrk);
		 */
        populateData();
    }
    private void inIt(){
        cityArray = new ArrayList<SpinnerData>();
        SpinnerData defaultData = new SpinnerData("0","Select Your City");
        cityArray.add(defaultData);

        areaArray = new ArrayList<SpinnerData>();
    }
    private  void getValues(){
        addressObj.setAdd_heading(tag.getText().toString());
        addressObj.setAddress(addresss.getText().toString());
        addressObj.setLandmark(landmark.getText().toString());
        addressObj.setPincode(pincode.getText().toString());
        addressObj.setCity(((SpinnerData)citySpinner.getSelectedItem()).getId());
        addressObj.setCircle(((SpinnerData) areaSpinner.getSelectedItem()).getId());
    }
    private void populateData(){
        tag.setText(addressObj.getAdd_heading());
        addresss.setText(addressObj.getAddress());
        landmark.setText(addressObj.getLandmark());
        pincode.setText(addressObj.getPincode());
    }

    private class c_cityAsyncTask extends AsyncTask<String, Void, String> {
        //ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
            //progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                // jsonObject.accumulate("p_date",date);
                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            JSONObject jsonResponse;
            int selectedIndex=0;
            try {
                if (!result.equals("")) {

                    JSONObject jsonObject = new JSONObject(result);
                    String status = jsonObject.optString("status");

                    if (status.equals("1")) {
                        JSONArray jArr = jsonObject.getJSONArray("data");
                        if (jArr != null) {
                            for (int i = 0; i < jArr.length(); i++) {
                                jsonResponse = jArr.getJSONObject(i);
                                String id = jsonResponse.optString("id").toString();
                                String name = jsonResponse.optString("city").toString();
                                SpinnerData data = new SpinnerData(id,name);
                                cityArray.add(data);

                                if(addressObj.getCity() != null && addressObj.getCity().equals(id)){
                                    selectedIndex=i+1;
                                }
                            }
                        }
                    }
                    loadSpinnerData(citySpinner,cityArray,selectedIndex,"city");
                }
                else{
                    Toast.makeText( Add_address.this, "Bad Network Connection !!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            //progress.setVisibility(View.GONE);

            super.onPostExecute(result);
        }

    }
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    private void loadSpinnerData(Spinner spinner,final ArrayList<SpinnerData> spinnerItems,int selectedIndex,final String type) {
        // TODO Auto-generated method stub

        // Creating adapter for spinner
        ArrayAdapter<SpinnerData> dataAdapter = new ArrayAdapter<SpinnerData>(
                Add_address.this, android.R.layout.simple_spinner_item,
                spinnerItems);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                if(type.equals("city")) {
                    newc_id = spinnerItems.get(position).getId();
                    new c_AreaAsyncTask().execute(Serverlinks.choose_area);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }

        });

        if(selectedIndex > 0)
            spinner.setSelection(selectedIndex);
    }

    private class c_AreaAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
			/*
			 * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("location_id", newc_id);
                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString1(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            areaArray.clear();
            SpinnerData defaultdata =new SpinnerData("0","Select Your Area");
            areaArray.add(defaultdata);

            int selectedIndex=0;
            JSONObject jsonResponse;

            try {
                if (!result.equals("")){
                    JSONObject jsonObject = new JSONObject(result);

                    String status = jsonObject.optString("status");

                    if (status.equals("1")) {
                        JSONArray jArr = jsonObject.getJSONArray("data");

                        if (jArr != null) {
                            for (int i = 0; i < jArr.length(); i++) {
                                jsonResponse = jArr.getJSONObject(i);
                                String id = jsonResponse.optString("id").toString();
                                String name = jsonResponse.optString("location_name").toString();
                                SpinnerData data = new SpinnerData(id,name);
                                areaArray.add(data);
                                if(addressObj.getCircle()!=null && addressObj.getCircle().equals(id)){
                                    selectedIndex = i+1;
                                }
                            }
                        }
                        loadSpinnerData(areaSpinner,areaArray,selectedIndex,"circle");
                    }

                }
                else{
                    Toast.makeText( Add_address.this, "Bad Network Connection !!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            catch (Exception e) {
                Log.e("String ", e.toString());
            }
            // progressDialog.dismiss();

            super.onPostExecute(result);
        }

    }
    private static String convertInputStreamToString1(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
    private class submit_addAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Add_address.this);
            progressDialog.show();

        }
        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", userId);
                jsonObject.accumulate("id",(addressObj.getAddr_id()==null)?"":addressObj.getAddr_id());
                jsonObject.accumulate("add_heading", addressObj.getAdd_heading());
                jsonObject.accumulate("city", addressObj.getCity());
                jsonObject.accumulate("circle", addressObj.getCircle());
                jsonObject.accumulate("address", addressObj.getAddress());
                jsonObject.accumulate("landmark", addressObj.getLandmark());
                jsonObject.accumulate("pincode", addressObj.getPincode());

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString5(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            if (progressDialog.isShowing())
                progressDialog.cancel();
            try {
                if (!result.equals("")){
                    JSONObject jObject = new JSONObject(result);
                    int status = jObject.getInt("status");
                    if (status == 0) {
                        Toast.makeText(getBaseContext(), "Insufficient Data", Toast.LENGTH_SHORT).show();
                    } else if (status == 1) {

                        Intent i = new Intent(Add_address.this,Profile_page.class);
                        startActivity(i);
                        finish();
                    }
                }
                else{
                    Toast.makeText( Add_address.this, "Bad Network Connection !!",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {

            }
            new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    progress.setVisibility(View.GONE);

                }
            };
            super.onPostExecute(result);
        }

    }
    private static String convertInputStreamToString5(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
