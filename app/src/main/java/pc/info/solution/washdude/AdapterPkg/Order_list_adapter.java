package pc.info.solution.washdude.AdapterPkg;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import pc.info.solution.washdude.OrderDetails;
import pc.info.solution.washdude.R;
import pc.info.solution.washdude.webservices.Serverlinks;

/**
 * Created by abhis on 8/1/2016.
 */
public class Order_list_adapter extends RecyclerView.Adapter<Order_list_adapter.trackHolder> {
  Context context;
    private String[] orderid;
    private String[] orderDate;
    private String[] icon;
    private int [] orderStatus;


    public Order_list_adapter(Context context,String[] orderid,String[] orderDate,int[] orderStatus,String[] icon){

        this.context = context ;
        this.orderStatus=orderStatus;
        this.orderid = orderid;
        this.orderDate=orderDate;
        this.icon=icon;

    }

    @Override
    public trackHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.each_track_item,parent,false);
        trackHolder holder = new trackHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(final trackHolder holder, final int position) {
        holder.orderid.setText("ORD ID: "+orderid[position]);
        holder.date.setText(orderDate[position]);
        holder.status.setText(Serverlinks.statusArray[orderStatus[position]]);
       /* Glide.with(context).load(icon.get(position)).thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.icon_service);*/

        Uri myUri = Uri.parse(icon[position]);
        Glide.with(context)
                .load(myUri)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .placeholder(R.drawable.delivery_truck)
                .thumbnail(0.5f)
                .into(holder.icon);

        holder.order_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(context, OrderDetails.class);
                i.putExtra("jsonorderId",orderid[position]);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderid.length;
    }

    public class trackHolder extends RecyclerView.ViewHolder{

        ImageView icon;
        TextView status , orderid,date ;
        RelativeLayout order_lay;

        public trackHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            status = (TextView) itemView.findViewById(R.id.status_order);
            orderid = (TextView) itemView.findViewById(R.id.order_id);
            date = (TextView) itemView.findViewById(R.id.order_date);
            order_lay = (RelativeLayout) itemView.findViewById(R.id.order_lay);
        }
    }
}
