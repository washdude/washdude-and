package pc.info.solution.washdude;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pc.info.solution.washdude.AdapterPkg.MemberAdpater;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.UTILLS.Plans;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

public class Membership extends AppCompatActivity {

    RecyclerView memberlist;
    MemberAdpater adpater;
    private SharedPreferenceClass sharedPreferenceClass;
    String userId;
    String key_usr = "user_id";
    TextView planText,noteText;
    String plan_staus = "0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        memberlist = (RecyclerView) findViewById(R.id.memberList);
        planText = (TextView) findViewById(R.id.plan_text);
        noteText = (TextView) findViewById(R.id.note);
        memberlist.setLayoutManager(new LinearLayoutManager(this));


        planText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (plan_staus.equals("0")) {

                    plan_staus = "1";

                    getPlanDataUser(userId, Serverlinks.MY_PLAN);


                } else {
                    plan_staus = "0";
                    getPlanData(userId, Serverlinks.All_PLAN);
                }

            }
        });
        getPlanData(userId, Serverlinks.All_PLAN);

    }


    public void getPlanData(final String userId, final String all_paln) {

        final ArrayList<Plans> planlist = new ArrayList<>();

        StringRequest jreq = new StringRequest(Request.Method.POST, all_paln, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("LIST OF PLANS", response.toString());


                try {

                    JSONObject jsonObj = new JSONObject(response);
                    JSONArray entry = jsonObj.getJSONArray("data");

                    for (int i = 0; i < entry.length(); i++) {

                        Plans plan = new Plans();
                        JSONObject json1 = entry.getJSONObject(i);
                        int plan_id = json1.getInt("id");
                        String plan_name = json1.getString("plan_name");
                        String price = json1.getString("price");
                        int used = json1.getInt("used");

                        plan.setPlan_id(plan_id);
                        plan.setPlan_name(plan_name);
                        plan.setPrice(price);
                        plan.setUsed(used);
                        planlist.add(plan);
                    }
                    adpater = new MemberAdpater(getBaseContext(), planlist,"all");
                    memberlist.setAdapter(adpater);

                } catch (Exception e) {


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userId);

                return params;
            }

        };
        MyApplication.getInstance().addToRequestQueue(jreq);


    }
    public void getPlanDataUser(final String userId, final String all_paln) {

        final ArrayList<Plans> planlistuser = new ArrayList<>();

        StringRequest jreq = new StringRequest(Request.Method.POST, all_paln, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("LIST OF PLANS", response.toString());



                try {


                    JSONObject jsonObj = new JSONObject(response);
                    int status=jsonObj.getInt("status");
                    if(status == 1) {
                        noteText.setVisibility(View.GONE);

                        JSONArray entry = jsonObj.getJSONArray("data");

                        for (int i = 0; i < entry.length(); i++) {

                            Plans plan = new Plans();
                            JSONObject json1 = entry.getJSONObject(i);
                            int plan_id = json1.getInt("id");
                            String plan_name = json1.getString("plan_name");
                            String price = json1.getString("price");
                            int used = json1.getInt("valid");

                            plan.setPlan_id(plan_id);
                            plan.setPlan_name(plan_name);
                            plan.setPrice(price);
                            plan.setUsed(used);
                            planlistuser.add(plan);
                        }


                        adpater = new MemberAdpater(getBaseContext(), planlistuser, "user");
                        memberlist.setAdapter(adpater);
                    }else if(status ==2)
                    {
                        noteText.setVisibility(View.VISIBLE);

                    }

                } catch (Exception e) {


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userId);

                return params;
            }

        };
        MyApplication.getInstance().addToRequestQueue(jreq);


    }

}
