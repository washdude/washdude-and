package pc.info.solution.washdude.AdapterPkg;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import pc.info.solution.washdude.Member_details;
import pc.info.solution.washdude.OrderDetails;
import pc.info.solution.washdude.R;
import pc.info.solution.washdude.UTILLS.Plans;

/**
 * Created by abhis on 8/3/2016.
 */
public class MemberAdpater extends RecyclerView.Adapter<MemberAdpater.PlanHolder> {
 Context context;
    ArrayList<Plans> planList;
    String planid,inputType;

    public MemberAdpater(Context context, ArrayList<Plans> planlist,String inputType){
        this.context = context;
        this.planList = planlist;
        this.inputType=inputType;

    }

    @Override
    public PlanHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.each_member_list,parent,false);
        PlanHolder holder = new PlanHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(PlanHolder holder, final int position) {

        if(inputType.equals("all")) {
            Plans plans = planList.get(position);


            holder.Plan_name.setText(planList.get(position).getPlan_name());
            holder.plan_price.setText("Rs " + planList.get(position).getPrice() + "/-");
            planid = "" + planList.get(position).getPlan_id();
            if(plans.getUsed()==1){
                holder.purchase_lay.setVisibility(View.VISIBLE);

            }else{
                holder.purchase_lay.setVisibility(View.GONE);
            }

        }else if(inputType.equals("user")){
            holder.Plan_name.setText(planList.get(position).getPlan_name());
            holder.plan_price.setText("Rs " + planList.get(position).getPrice() + "/-");
            planid = "" + planList.get(position).getPlan_id();
            holder.purchase_lay.setVisibility(View.GONE);


        }
        holder.member_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(context, Member_details.class);
                i.putExtra("jsonplanId",planid);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return planList.size();
    }


    public class PlanHolder extends RecyclerView.ViewHolder{

       TextView Plan_name,plan_price,plan_details;
        RelativeLayout purchase_lay,member_lay;


        public PlanHolder(View itemView) {
            super(itemView);

            Plan_name = (TextView) itemView.findViewById(R.id.plan_name);
            plan_details = (TextView) itemView.findViewById(R.id.plan_desc);
            plan_price = (TextView) itemView.findViewById(R.id.plan_price);
            purchase_lay = (RelativeLayout) itemView.findViewById(R.id.purchase_lay);
            member_lay = (RelativeLayout) itemView.findViewById(R.id.relMember);
        }
    }
}
