package pc.info.solution.washdude.AdapterPkg;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import pc.info.solution.washdude.ChooseSlot;
import pc.info.solution.washdude.R;
import pc.info.solution.washdude.UTILLS.ProfileAddress;

/**
 * Created by abhis on 7/29/2016.
 */
public class ChooseAdressAdapter extends RecyclerView.Adapter<ChooseAdressAdapter.chooseHolder> {
    Context context;
    private List<ProfileAddress> profAddr;
    public int mSelectedItem = -1;

    public ChooseAdressAdapter(Context context, List<ProfileAddress> profAddr) {
        this.context = context;
        this.profAddr = profAddr;

    }

    @Override
    public chooseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.choose_address_item, parent, false);
        chooseHolder holder = new chooseHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final chooseHolder holder, final int position) {

        final ProfileAddress address = profAddr.get(position);
        holder.tag.setText(address.getAdd_heading());
        holder.area.setText(address.getCircle_name());
        holder.adress.setText(address.getAddress() + "," + address.getPincode());


        holder.rb.setChecked(position == mSelectedItem);

        holder.rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectedItem = holder.getAdapterPosition();
                notifyItemRangeChanged(0, profAddr.size());
                ChooseSlot.adress_text = address;
                ChooseSlot.isAddressSelect = true;



            }
        });




    }


    @Override
    public int getItemCount() {
        return profAddr.size();
    }

    public class chooseHolder extends RecyclerView.ViewHolder {

        AppCompatRadioButton rb;
        TextView tag, area, adress;
        ImageView cross, edit;


        public chooseHolder(View itemView) {
            super(itemView);

            rb = (AppCompatRadioButton) itemView.findViewById(R.id.rb_button);
            tag = (TextView) itemView.findViewById(R.id.tag_id);
            area = (TextView) itemView.findViewById(R.id.area_id);
            adress = (TextView) itemView.findViewById(R.id.adress_id);




        }
    }
}
