package pc.info.solution.washdude;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pc.info.solution.washdude.AdapterPkg.SummeryListAdapter;
import pc.info.solution.washdude.UTILLS.Catagories;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.UTILLS.DatabaseHandler;
import pc.info.solution.washdude.UTILLS.PrefManager;
import pc.info.solution.washdude.UTILLS.TypefaceSpan;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

public class SummaryPage extends AppCompatActivity {
    String couponcode;
    RecyclerView summerList;
    SummeryListAdapter adapter;
    List<Catagories> summery_list;
    List<Catagories> cat;

    DatabaseHandler db;
    PrefManager prefManager;

    private Boolean isRemoveCoupon=false;
    float sum;
    TextView clothcount, price, totPrice,pickup_slot_dt,deliv_slot_dt,addresstext,coupon_text,sum_price,coupon_discount_Rs;
    RelativeLayout proceed_summary, notaion_lay;
    TextView notn_txt,couponAmnt;


    TextView apply_cupon;
    String  pickup_date = "", pickup_addrid = "", pickup_slot = "", delivery_date = "",
            delivery_addrid = "", delivery_slot = "",
            order_price, total_price,pickup_slot_name="",delivery_slot_name="",address="",pincode="";

    String serviceCallMode;
    String orderId;
    Context activityContext;
    private String userId;
    private SharedPreferenceClass sharedPreferenceClass;
    CardView apply_coupon;
    EditText edtCouponcode;
    int count;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityContext = this;
        setContentView(R.layout.activity_summery_actibve);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        notaion_lay = (RelativeLayout) findViewById(R.id.notaion_lay);
        notn_txt = (TextView) findViewById(R.id.cpn_notn_text);
        clothcount = (TextView) findViewById(R.id.total_id);
        price = (TextView) findViewById(R.id.tot_price_id);
        totPrice = (TextView) findViewById(R.id.whtotal_price);
        pickup_slot_dt = (TextView) findViewById(R.id.pick_date_time);
        deliv_slot_dt = (TextView) findViewById(R.id.deliv_pick_time);
        addresstext = (TextView) findViewById(R.id.adress_detl);
        coupon_text = (TextView) findViewById(R.id.coupon_text);
        edtCouponcode = (EditText) findViewById(R.id.cpn_edit);
        apply_coupon = (CardView) findViewById(R.id.apply_cpn);
        apply_cupon = (TextView) findViewById(R.id.apply_coupon);
        sum_price = (TextView) findViewById(R.id.tot_price_id);
        coupon_discount_Rs  = (TextView) findViewById(R.id.coupon_discount_Rs);
        couponAmnt  = (TextView) findViewById(R.id.cpn_value);

        proceed_summary = (RelativeLayout) findViewById(R.id.proceed_summary);


        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);

        prefManager = new PrefManager(this,userId);

        delivery_date=getIntent().getStringExtra("delivery_date");
        delivery_addrid=getIntent().getStringExtra("delivery_addrid");
        delivery_slot=getIntent().getStringExtra("delivery_slot");
        pickup_date=getIntent().getStringExtra("pickup_date");
        pickup_addrid=getIntent().getStringExtra("pickup_addrid");
        pickup_slot=getIntent().getStringExtra("pickup_slot");
        pickup_slot_name=getIntent().getStringExtra("pickup_slot_name");
        delivery_slot_name=getIntent().getStringExtra("delivery_slot_name");
        address=getIntent().getStringExtra("address");
        pincode=getIntent().getStringExtra("pin");


        SpannableString s = new SpannableString("SUMMARY");
        s.setSpan(new TypefaceSpan(this, "AvenirLTStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(s);
        summerList = (RecyclerView) findViewById(R.id.summery_list);

        pickup_slot_dt.setText(pickup_date +","+pickup_slot_name);
        deliv_slot_dt.setText(delivery_date +","+delivery_slot_name);
        addresstext.setText(address +","+pincode);


        apply_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isRemoveCoupon) {
                    couponcode = edtCouponcode.getText().toString();
                    if (couponcode.equals("")) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SummaryPage.this);
                        alertDialogBuilder.setMessage("make Sure you entered a coupon code or not?");

                        alertDialogBuilder.setNegativeButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {
                        saveOrder("coupon");


                    }
                    coupon_text.setText(couponcode);
                }
                else {
                    isRemoveCoupon=false;
                    apply_cupon.setText("APPLY");
                    removeCoupon();
                }

            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        db = new DatabaseHandler(getBaseContext());
        summery_list = db.getAllcatagory();
        cat = new ArrayList<Catagories>();


        for (int i = 0; i < summery_list.size(); i++) {
            if (summery_list.get(i).getTotal().equals("0") && summery_list.get(i).getPrice().equals("0.0")) {
            } else {
                cat.add(summery_list.get(i));
            }
        }


 /*       Gson gson = new Gson();
        Log.d("summarylist",gson.toJson(summerList).toString());*/

        for (int i = 0; i < cat.size(); i++) {
            sum = sum + Float.parseFloat(cat.get(i).getPrice());
            count = count + Integer.parseInt(cat.get(i).getTotal());
        }

       price.setText(String.valueOf(sum)+"0/-");
        totPrice.setText(String.valueOf(sum)+"0/-");
        //price.setText(String.valueOf(sum));
        //totPrice.setText(String.valueOf(sum));
        clothcount.setText(String.valueOf(count));

        summerList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SummeryListAdapter(getBaseContext(), cat);
        summerList.setAdapter(adapter);

        proceed_summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveOrder("proceed");
            }
        });
    }

    private void saveOrder(String callType) {
        total_price=sum_price.getText().toString();
        if(callType.equals("proceed")){
            total_price=sum_price.getText().toString();
            String discountPrice=coupon_discount_Rs.getText().toString();
            if(discountPrice.equals("")){
                order_price=total_price;
                couponcode="";
            }else{
                Log.d("Summary_order","Inside saveOrder else part");

                //order_price=""+(Double.parseDouble(total_price) +Double.parseDouble(discountPrice));
                order_price=totPrice.getText().toString(); ;
                couponcode=coupon_text.getText().toString();
            }
        }

        if (cat==null){
            Toast.makeText(SummaryPage.this, "There is no data..", Toast.LENGTH_SHORT).show();
        } else {
            serviceCallMode=callType;
        new SubmitTask().execute((callType.equals("proceed")) ? Serverlinks.saveorder : Serverlinks.coupon);
        }
    }

    private class SubmitTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (serviceCallMode.equals("coupon")) {
                progressDialog = new ProgressDialog(activityContext);
                progressDialog.setMessage("Applying Coupon");
                progressDialog.show();
//            progress.setVisibility(View.VISIBLE);
            } else if (serviceCallMode.equals("proceed")) {
                progressDialog = new ProgressDialog(activityContext);
                progressDialog.show();

            }
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                HashMap<String, Object> dataMap = new HashMap<String, Object>();
                dataMap.put("user_id", userId);
                dataMap.put("pickup_date", pickup_date);
                dataMap.put("pickup_addrid", pickup_addrid);
                dataMap.put("pickup_slotid", pickup_slot);
                dataMap.put("deliv_date", delivery_date);
                dataMap.put("deliv_addrid", delivery_addrid);
                dataMap.put("deliv_slotid", delivery_slot);
                dataMap.put("order_price", order_price);
                dataMap.put("total_price", total_price);
                dataMap.put("coupon_code", couponcode);
                dataMap.put("clothlist", cat);
                //Log.d(TAG,dataMap.toString());
                json = new Gson().toJson(dataMap);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            JSONObject jsonResponse;
            if (progressDialog.isShowing())
                progressDialog.cancel();

            try {
                if (!result.equals("")) {

                    Log.d("Summary_order", result);
                    JSONObject jsonObject = new JSONObject(result);
                    String status = jsonObject.optString("status");
                    if (status != null) {
                        if (serviceCallMode.equals("proceed") && status.equals("1")) {
                            orderId = jsonObject.optString("order_id");
                            db.delete_data();
                            prefManager.clearData();

                            Intent i = new Intent(SummaryPage.this, Confirmed.class);
                            i.putExtra("jsonorderId", orderId);
                            startActivity(i);
                            finish();

                        } else if (serviceCallMode.equals("coupon")) {
                            String data = jsonObject.getString("data");
                            if (status.equals("1")) {
                                JSONObject obj = new JSONObject(data);
                                String discount_amount = obj.getString("discount_amount");
                                String billing_amount = obj.getString("billing_amount");
                                String total_price = obj.getString("total_price");
                                String msg = obj.getString("msg");
                                //applyCoupon(Double.parseDouble(discount_amount), Double.parseDouble(billing_amount),Double.parseDouble(total_price),msg);
                                applyCoupon(discount_amount, billing_amount,total_price,msg);


                            } else {
                                apply_coupon.setEnabled(true);
                                AlertDialog.Builder builder = new AlertDialog.Builder(SummaryPage.this);
                                builder.setMessage(data)
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                                //edtCouponcode.setText("");
                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }

                        }
                    } else {

					/*Toast.makeText(Addlocationnew.this,
                            "Something is going wrong!!!.", Toast.LENGTH_LONG).show();*/
                    }
                }

            } catch (Exception e) {
                Log.e("String ", e.toString());
            }
            //progress.setVisibility(View.GONE);

            super.onPostExecute(result);
        }

    }

    private void applyCoupon(String discountAmnt, String billingAmnt,String tot,String msg) {

        coupon_text.setText(edtCouponcode.getText().toString());
        coupon_discount_Rs.setText(discountAmnt);
        sum_price.setText(billingAmnt);

        couponAmnt.setText(discountAmnt+"/-");
        price.setText(tot);
        totPrice.setText(billingAmnt+"/-");
        //totPrice.setText(billingAmnt);

        apply_coupon.setEnabled(false);
        apply_cupon.setText("CANCEL");
        apply_coupon.setBackgroundColor(Color.parseColor("#ffa500"));
        notaion_lay.setVisibility(View.VISIBLE);
        notn_txt.setText(msg);
        isRemoveCoupon=true;
    }
    private void removeCoupon(){
        String couponDiscount=coupon_discount_Rs.getText().toString();
        String sumPrice=sum_price.getText().toString();
        coupon_text.setText("");

        Double total=Double.parseDouble(couponDiscount) + Double.parseDouble(sumPrice);
        sum_price.setText(total +"");
        coupon_discount_Rs.setText("0");

        edtCouponcode.setText("");
    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}

