package pc.info.solution.washdude;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import pc.info.solution.washdude.AdapterPkg.ChooseAdressAdapter;
import pc.info.solution.washdude.AdapterPkg.TimeSlotAdapter;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.UTILLS.ProfileAddress;
import pc.info.solution.washdude.UTILLS.TypefaceSpan;
import pc.info.solution.washdude.webservices.AllStaticVariables;
import pc.info.solution.washdude.webservices.AsynkTaskCommunicationInterface;
import pc.info.solution.washdude.webservices.AsynkTaskForServerCommunication;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

public class ChooseSlot extends AppCompatActivity implements AsynkTaskCommunicationInterface, TimeSlotAdapter.TimeSlotSelectedListener {

    public static RecyclerView choose_adressview;
    public static ChooseAdressAdapter adapter;
    RelativeLayout pick_slot, deliv_slot,pick_slot_,deliv_slot_,add_slot_add;
    RelativeLayout next;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    private SharedPreferenceClass sharedPreferenceClass;
    private String userId;
    List<ProfileAddress> profileAddresses;
    private String[] dates;
    private static final String TAG = "ChooseSlot";
    private ArrayList<String> addslotIdPickup, addslotDescPickup;
    private ArrayList<String> addslotIdDelivery, addslotDescDelivery;
    private Boolean isDelivery = false;
    private Boolean isPickUpSelected = false;
    private Boolean isDeliverySelected = false;
    public static   ProfileAddress adress_text;
    CoordinatorLayout coordinatorLayout;
    public static boolean isAddressSelect = false;
    public static String slotId, slotDesc;
    TextView noslot;
    private int selectedPickupDateIndex = 0;

    String pickupline, deliveryline, selectedPickupTime, selectedDeliveryTime, selectedSlotIdPickup, selectedSlotIdDelivery, selectedPickupDate, selectedDeliveryDate;

    RecyclerView slotvew;
    TextView pickText, deliverText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_slot);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordimator);
        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);

        pick_slot = (RelativeLayout) findViewById(R.id.tick_lay1);
        deliv_slot = (RelativeLayout) findViewById(R.id.tick_lay2);
        pick_slot_ = (RelativeLayout) findViewById(R.id.first_tick);
        deliv_slot_ = (RelativeLayout) findViewById(R.id.second_tick);
        add_slot_add = (RelativeLayout) findViewById(R.id.ad_id);

        add_slot_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChooseSlot.this, Add_address.class);
                startActivity(intent);
                finish();
            }
        });

        next = (RelativeLayout) findViewById(R.id.next_layout);
        pickText = (TextView) findViewById(R.id.pick_text);
        deliverText = (TextView) findViewById(R.id.deliv_text);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString("Choose Address & Slot");
        s.setSpan(new TypefaceSpan(this, "AvenirLTStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(s);
        choose_adressview = (RecyclerView) findViewById(R.id.adress_title_listview);


        pick_slot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isDelivery = false;
                showDialog(getPickupDates());

            }
        });

        deliv_slot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isPickUpSelected) {
                    isDelivery = true;
                    showDialog(getDeliveryDates(selectedPickupDateIndex + 2));
                } else {
                    Toast.makeText(getApplicationContext(), "Please select pickup date and time first", Toast.LENGTH_SHORT);
                }


            }
        });



        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((isPickUpSelected) && (isDeliverySelected) && isAddressSelect) {
                    Log.d(TAG, "selectedSlotIdPickup : " + selectedSlotIdPickup + " selectedSlotIdDelivery :" + selectedSlotIdDelivery);
                    Log.d(TAG, "selectedPickupDate : " + selectedPickupDate + " selectedDeliveryDate :" + selectedDeliveryDate);
                    Log.d(TAG, "selectedPickupTime : " + selectedPickupTime + " selectedDeliveryTime :" + selectedDeliveryTime);


                    Intent i1 = new Intent(ChooseSlot.this, SummaryPage.class);
                    i1.putExtra("delivery_date", selectedDeliveryDate);
                    i1.putExtra("delivery_addrid", adress_text.getAddr_id());
                    i1.putExtra("delivery_slot", selectedSlotIdDelivery);
                    i1.putExtra("pickup_date", selectedPickupDate);
                    i1.putExtra("pickup_addrid", adress_text.getAddr_id());
                    i1.putExtra("pickup_slot", selectedSlotIdPickup);
                    i1.putExtra("pickup_slot_name", selectedPickupTime);
                    i1.putExtra("delivery_slot_name", selectedDeliveryTime);
                    i1.putExtra("address", adress_text.getAddress());
                    i1.putExtra("pin", adress_text.getPincode());
                    startActivity(i1);
                    finish();

                } else{

                    Snackbar snackbar = Snackbar.
                            make(coordinatorLayout,"Please select Pickup ,Delivery Date and an Adress before proceeding",Snackbar.LENGTH_LONG);
                    snackbar.show();

                }




            }
        });

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("user_id", userId);
        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, Serverlinks.user_profile);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(ChooseSlot.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);

    }

    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }

    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if (jsonObject != null) {
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status = jsonObject.getInt("status");
                String data = jsonObject.getString("data");
                //Create a jsonObject of String data
                JSONObject dataObject = new JSONObject(data);
                if (status == 0) {
                    Toast.makeText(ChooseSlot.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                } else if (status == 1) {


                    add_slot_add.setVisibility(View.GONE);
                    final JSONArray jArr = dataObject.getJSONArray("address");
                    profileAddresses = new Gson().fromJson(jArr.toString(), new TypeToken<ArrayList<ProfileAddress>>() {
                    }.getType());
                    if (profileAddresses == null)
                        profileAddresses = new ArrayList<ProfileAddress>();
                    /*txtAdr.setVisibility(View.VISIBLE);
                    txtAdr.setText("Please add address for order");
                    if(!(profileAddresses.size()==0)){
                        txtAdr.setVisibility(View.INVISIBLE);
                    }*/
                    choose_adressview.setLayoutManager(new LinearLayoutManager(this));
                    adapter = new ChooseAdressAdapter(ChooseSlot.this, profileAddresses);
                    choose_adressview.addItemDecoration(new SpacesItemDecoration(20));
                    choose_adressview.setAdapter(adapter);// set adapter


                }else {

                    add_slot_add.setVisibility(View.VISIBLE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void showDialog(final String[] dateArray) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ChooseSlot.this);
        LayoutInflater inflater = getLayoutInflater();
        View view1 = inflater.inflate(R.layout.time_date_slot, null);
        builder.setView(view1);


        final TextView dt1, dt2, dt3, dt4, dt5, dt6, dt7, set;

        final ImageView cross;
        dt1 = (TextView) view1.findViewById(R.id.dt_1);
        dt2 = (TextView) view1.findViewById(R.id.dt_2);
        dt3 = (TextView) view1.findViewById(R.id.dt_3);
        dt4 = (TextView) view1.findViewById(R.id.dt_4);
        dt5 = (TextView) view1.findViewById(R.id.dt_5);
        dt6 = (TextView) view1.findViewById(R.id.dt_6);
        dt7 = (TextView) view1.findViewById(R.id.dt_7);
        noslot = (TextView) view1.findViewById(R.id.slotDesc);
        dt3.setText(dateArray[2]);
        dt4.setText(dateArray[3]);
        dt5.setText(dateArray[4]);
        dt6.setText(dateArray[5]);
        dt7.setText(dateArray[6]);
        if (isDelivery) {
            dt1.setText(dateArray[0]);
            dt2.setText(dateArray[1]);
        }


        set = (TextView) view1.findViewById(R.id.set_time);
        cross = (ImageView) view1.findViewById(R.id.cross_);

        slotvew = (RecyclerView) view1.findViewById(R.id.slotView);


        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isDelivery) {
                    if (isPickUpSelected) {
                        pickText.setText(selectedPickupDate +","+selectedPickupTime);
                        pick_slot_.setBackgroundResource(R.drawable.edit_solid_background);
                        alertDialog.dismiss();
                    }
                    else {
                        Toast.makeText(getBaseContext(), "Please select some pickup time slot", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (isDeliverySelected) {
                        deliverText.setText(selectedDeliveryDate +","+selectedDeliveryTime);
                        deliv_slot_.setBackgroundResource(R.drawable.edit_solid_background);
                        alertDialog.dismiss();
                    }
                    else {
                        deliverText.setText(selectedDeliveryDate);
                        Toast.makeText(getBaseContext(), "Please select some delivery time slot", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        getTimeSlot(dateArray[0]);
        if (isDelivery)
            selectedDeliveryDate = dateArray[0];
        else
            selectedPickupDate = dateArray[0];

        dt1.setBackgroundColor(getResources().getColor(R.color.light_green_but_dark));
        dt2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        dt3.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        dt4.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        dt5.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        dt6.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        dt7.setBackgroundColor(getResources().getColor(R.color.colorPrimary));



        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        dt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTimeSlot(dateArray[0]);
                if (!isDelivery) {
                    selectedPickupDate = dateArray[0];
                    selectedPickupDateIndex = 0;
                } else {
                    selectedDeliveryDate = dateArray[0];
                }
                dt1.setBackgroundColor(getResources().getColor(R.color.light_green_but_dark));
                dt2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt3.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt4.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt5.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt6.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt7.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            }
        });
        dt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTimeSlot(dateArray[1]);
                if (!isDelivery) {
                    selectedPickupDate = dateArray[1];
                    selectedPickupDateIndex = 1;

                } else {
                    selectedDeliveryDate = dateArray[1];
                }
                dt1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt2.setBackgroundColor(getResources().getColor(R.color.light_green_but_dark));
                dt3.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt4.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt5.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt6.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt7.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            }
        });
        dt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTimeSlot(dateArray[2]);
                if (!isDelivery) {
                    selectedPickupDate = dateArray[2];
                    selectedPickupDateIndex = 2;
                } else {
                    selectedDeliveryDate = dateArray[2];
                }
                dt1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt3.setBackgroundColor(getResources().getColor(R.color.light_green_but_dark));
                dt4.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt5.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt6.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt7.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            }
        });
        dt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTimeSlot(dateArray[3]);
                if (!isDelivery) {
                    selectedPickupDate = dateArray[3];
                    selectedPickupDateIndex = 3;
                } else {
                    selectedDeliveryDate = dateArray[3];
                }
                dt1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt3.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt4.setBackgroundColor(getResources().getColor(R.color.light_green_but_dark));
                dt5.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt6.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt7.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            }
        });
        dt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTimeSlot(dateArray[4]);
                if (!isDelivery) {
                    selectedPickupDate = dateArray[4];
                    selectedPickupDateIndex = 4;

                } else {
                    selectedDeliveryDate = dateArray[4];
                }
                dt1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt3.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt4.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt5.setBackgroundColor(getResources().getColor(R.color.light_green_but_dark));
                dt6.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt7.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            }
        });
        dt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTimeSlot(dateArray[5]);
                if (!isDelivery) {
                    selectedPickupDate = dateArray[5];
                    selectedPickupDateIndex = 5;
                } else {
                    selectedDeliveryDate = dateArray[5];
                }
                dt1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt3.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt4.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt5.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt6.setBackgroundColor(getResources().getColor(R.color.light_green_but_dark));
                dt7.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            }
        });
        dt7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTimeSlot(dateArray[6]);
                if (!isDelivery) {
                    selectedPickupDate = dateArray[6];
                    selectedPickupDateIndex = 6;

                } else {
                    selectedDeliveryDate = dateArray[6];
                }
                dt1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt3.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt4.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt5.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt6.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                dt7.setBackgroundColor(getResources().getColor(R.color.light_green_but_dark));
            }
        });


    }

    @Override
    public void onTimeSlotSelected(int selectedIndex) {
        int temp = selectedIndex;
        if (!isDelivery) {
            selectedSlotIdPickup = addslotIdPickup.get(selectedIndex);
            selectedPickupTime = addslotDescPickup.get(selectedIndex);

            Log.d(TAG, "Selected Slot Desc : " + addslotDescPickup.get(selectedIndex) + " selectedSlotIdPickup :" + selectedSlotIdPickup);
            isPickUpSelected = true;
        } else {
            selectedSlotIdDelivery = addslotIdDelivery.get(selectedIndex);
            selectedDeliveryTime = addslotDescDelivery.get(selectedIndex);
            Log.d(TAG, "Selected Slot Desc : " + addslotDescPickup.get(selectedIndex) + " selectedSlotIdDelivery :" + selectedSlotIdDelivery);
            isDeliverySelected = true;
        }
    }


    private void getTimeSlot(String date) {
        if (!isDelivery)
            isPickUpSelected = false;
        else
            isDeliverySelected = false;
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Time Slot Available");
        progressDialog.show();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("date", date);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Serverlinks.timeslot, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        Log.e(TAG, "TimeSlot Response: " + response);

                        if (response != null) {

                            handleTimeSlotResponse(response);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Log.e(TAG, "Error: " + error.getMessage());
                }
            });
            MyApplication.getInstance().addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleTimeSlotResponse(JSONObject response) {
        try {
            ArrayList<String> addslotId, addslotDesc;
            addslotId = new ArrayList<>();
            addslotDesc = new ArrayList<>();

            int status = response.getInt("status");
            if (status == 0) {


                slotvew.setVisibility(View.GONE);
                noslot.setVisibility(View.VISIBLE);

            } else if (status == 1) {

                noslot.setVisibility(View.GONE);
                slotvew.setVisibility(View.VISIBLE);
                JSONArray data = response.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject dataObject = data.getJSONObject(i);
                    slotId = dataObject.getString("slot_id");
                    slotDesc = dataObject.getString("from_to");

                    addslotId.add(slotId);
                    addslotDesc.add(slotDesc);

                    Log.d(TAG, slotDesc);
                }
                if (!isDelivery) {
                    addslotIdPickup = addslotId;
                    addslotDescPickup = addslotDesc;
                } else {
                    addslotIdDelivery = addslotId;
                    addslotDescDelivery = addslotDesc;
                }
                TimeSlotAdapter slotAdapter = new TimeSlotAdapter(getBaseContext(), addslotId, addslotDesc, this);
                slotvew.setLayoutManager(new LinearLayoutManager(ChooseSlot.this));
                slotvew.setAdapter(slotAdapter);
                slotAdapter.notifyDataSetChanged();

            } else if (status == 2){
                slotvew.setVisibility(View.GONE);
                noslot.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {


            // Add top margin only for the first item to avoid double space between items
            if (parent.getChildPosition(view) == 0)
                outRect.top = space + space;

        }
    }

    private String[] getPickupDates() {
        String[] dates = new String[7];

///today
        Calendar calendar = Calendar.getInstance();
        Date day1 = calendar.getTime();
        String day_1 = getDate(day1);
        dates[0] = (day_1);

///tomorrow
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.DAY_OF_YEAR, 1);
        Date day2 = c1.getTime();
        String day_2 = getDate(day2);
        dates[1] = (day_2);
///day3

        Calendar c2 = Calendar.getInstance();
        c2.add(Calendar.DAY_OF_YEAR, 2);
        Date day3 = c2.getTime();
        String day_3 = getDate(day3);
        dates[2] = (day_3);
///day4

        Calendar c3 = Calendar.getInstance();
        c3.add(Calendar.DAY_OF_YEAR, 3);
        Date day4 = c3.getTime();
        String day_4 = getDate(day4);
        dates[3] = day_4;

////day5
        Calendar c4 = Calendar.getInstance();
        c4.add(Calendar.DAY_OF_YEAR, 4);
        Date day5 = c4.getTime();
        String day_5 = getDate(day5);
        dates[4] = (day_5);


//day6
        Calendar c5 = Calendar.getInstance();
        c5.add(Calendar.DAY_OF_YEAR, 5);
        Date day6 = c5.getTime();
        String day_6 = getDate(day6);
        dates[5] = (day_6);
///day7
        Calendar c6 = Calendar.getInstance();
        c6.add(Calendar.DAY_OF_YEAR, 6);
        Date day7 = c6.getTime();
        String day_7 = getDate(day7);
        dates[6] = (day_7);

        return dates;
    }

    private String[] getDeliveryDates(int selectedPickupDateIndex) {


        String[] dates = new String[7];

///today
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, selectedPickupDateIndex);
        Date day1 = calendar.getTime();
        String day_1 = getDate(day1);
        dates[0] = (day_1);

///tomorrow
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.DAY_OF_YEAR, selectedPickupDateIndex + 1);
        Date day2 = c1.getTime();
        String day_2 = getDate(day2);
        dates[1] = (day_2);

///day3

        Calendar c2 = Calendar.getInstance();
        c2.add(Calendar.DAY_OF_YEAR, selectedPickupDateIndex + 2);
        Date day3 = c2.getTime();
        String day_3 = getDate(day3);
        dates[2] = (day_3);
///day4

        Calendar c3 = Calendar.getInstance();
        c3.add(Calendar.DAY_OF_YEAR, selectedPickupDateIndex + 3);
        Date day4 = c3.getTime();
        String day_4 = getDate(day4);
        dates[3] = day_4;

////day5
        Calendar c4 = Calendar.getInstance();
        c4.add(Calendar.DAY_OF_YEAR, selectedPickupDateIndex + 4);
        Date day5 = c4.getTime();
        String day_5 = getDate(day5);
        dates[4] = (day_5);


//day6
        Calendar c5 = Calendar.getInstance();
        c5.add(Calendar.DAY_OF_YEAR, selectedPickupDateIndex + 5);
        Date day6 = c5.getTime();
        String day_6 = getDate(day6);
        dates[5] = (day_6);
///day7
        Calendar c6 = Calendar.getInstance();
        c6.add(Calendar.DAY_OF_YEAR, selectedPickupDateIndex + 6);
        Date day7 = c6.getTime();
        String day_7 = getDate(day7);
        dates[6] = (day_7);

        return dates;
    }

    public String getDate(Date date) {

        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

        String dateString = dateFormat.format(date);

        return dateString;

    }

}
