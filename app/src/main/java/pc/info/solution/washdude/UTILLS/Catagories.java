package pc.info.solution.washdude.UTILLS;

/**
 * Created by Abhishek on 4/28/2016.
 */
public class Catagories  {

    int id;
    String cloth_name ;
    String wash_type ;
    String total;
    String price;

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    String short_name;


    public String getServiconUrl() {
        return serviconUrl;
    }

    public void setServiconUrl(String serviconUrl) {
        this.serviconUrl = serviconUrl;
    }

    public String getCatgoryicon() {
        return catgoryicon;
    }

    public void setCatgoryicon(String catgoryicon) {
        this.catgoryicon = catgoryicon;
    }

    String serviconUrl;
    String catgoryicon;

    String clothId;

    public String getWashId() {
        return washId;
    }

    public void setWashId(String washId) {
        this.washId = washId;
    }

    String washId;
    public String getClothId() {
        return clothId;
    }

    public void setClothId(String clothId) {
        this.clothId = clothId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCloth_name() {
        return cloth_name;
    }

    public void setCloth_name(String cloth_name) {
        this.cloth_name = cloth_name;
    }

    public String getWash_type() {
        return wash_type;
    }

    public void setWash_type(String wash_type) {
        this.wash_type = wash_type;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
