package pc.info.solution.washdude;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pc.info.solution.washdude.AdapterPkg.MemberAdpater;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.UTILLS.Plans;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

public class ReferEarn extends AppCompatActivity {

    TextView refreCode,price;
    RelativeLayout invite;
    private SharedPreferenceClass sharedPreferenceClass;
    private String refBy;
    String userId;
    static ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_earn);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);
        refBy = sharedPreferenceClass.getValue_string(Constants.ref_code);

        refreCode = (TextView) findViewById(R.id.refer_text);
        price = (TextView) findViewById(R.id.textView2);
        invite = (RelativeLayout) findViewById(R.id.invite);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(this);
        getSupportActionBar().setTitle("Refer and Earn");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (refBy != null){
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=pc.info.solution.washdude" + "\n" + "Use this Referal code: " + refBy);
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);

                }
                else{
                    Toast.makeText(getBaseContext(),"You have no refferal code. Please Contact to washdude service care.", Toast.LENGTH_LONG).show();
                }

            }
        });

        getReferData(userId, Serverlinks.refercode);

    }
    public void getReferData(final String userId, final String refer) {
        progressDialog.setMessage("Fetching Refer Code");
        progressDialog.show();


        StringRequest jreq = new StringRequest(Request.Method.POST, refer, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("Refer", response.toString());
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                try {
                    JSONObject obj = new JSONObject(response);
                    String data = obj.getString("data");
                    JSONObject dtaObject = new JSONObject(data);

                    String refStr = dtaObject.getString("ref_code");
                    String priceStr = dtaObject.getString("price");

                    refreCode.setText(refStr);
                    price.setText("Refer a friend and earn credits worth Rs"+priceStr+".00/- when they transact");

                } catch (Exception e) {


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();


            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userId);

                return params;
            }

        };
        MyApplication.getInstance().addToRequestQueue(jreq);


    }

}
