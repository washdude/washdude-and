package pc.info.solution.washdude.AdapterPkg;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import pc.info.solution.washdude.R;
import pc.info.solution.washdude.UTILLS.OrderDetailsVO;
/**
 * Created by abhis on 8/3/2016.
 */
public class Orderdetails_adapter extends RecyclerView.Adapter<Orderdetails_adapter.Myholder>{

    Context context;
    private List<OrderDetailsVO> orderDetailsVOList;
    public Orderdetails_adapter(Context context,List<OrderDetailsVO> orderDetailsVOList){
        this.context = context;
        this.orderDetailsVOList=orderDetailsVOList;

    }

    @Override
    public Myholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.each_summary_layout,parent,false);
        Myholder holder = new Myholder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(Myholder holder, int position) {
        holder.cloth.setText(orderDetailsVOList.get(position).getCloth());
        //holder.services.setText(orderDetailsVOList.get(position).getService());
        holder.qty.setText(orderDetailsVOList.get(position).getQuantity());
        holder.price.setText(orderDetailsVOList.get(position).getUnitPrice());
        holder.service_ic.setText(orderDetailsVOList.get(position).getShtName());
        Glide.with(context).load(orderDetailsVOList.get(position).getParentImg())
                .thumbnail(0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.catagory_ic);

        if (position==orderDetailsVOList.size()-1){
            holder.dvdr_gr.setVisibility(View.GONE);

        }

    }

    @Override
    public int getItemCount() {
        return orderDetailsVOList.size();
    }

    public class Myholder extends RecyclerView.ViewHolder{

        TextView cloth,qty,price ,service_ic;
        ImageView catagory_ic;
        RelativeLayout dvdr_gr;

        public Myholder(View itemView) {
            super(itemView);
            cloth = (TextView) itemView.findViewById(R.id.cloth_ic);
            catagory_ic = (ImageView) itemView.findViewById(R.id.catagory_icon);
            service_ic = (TextView) itemView.findViewById(R.id.service_icon);
            qty = (TextView) itemView.findViewById(R.id.qty_ic);
            price = (TextView) itemView.findViewById(R.id.price_ic);
            dvdr_gr = (RelativeLayout) itemView.findViewById(R.id.dvdr_gr);
        }
    }
}
