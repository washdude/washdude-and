package pc.info.solution.washdude.AdapterPkg;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import pc.info.solution.washdude.R;
import pc.info.solution.washdude.UTILLS.Catagories;

/**
 * Created by abhis on 7/27/2016.
 */
public class SummeryListAdapter extends RecyclerView.Adapter<SummeryListAdapter.ListHolder> {

    Context context;
    List<Catagories> summeryList;

    public SummeryListAdapter(Context context, List<Catagories> summeryList){
        this.context = context;
        this.summeryList =summeryList;

    }

    @Override
    public ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.each_summary_layout,parent,false);
        ListHolder holder = new ListHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(ListHolder holder, int position) {

        Log.d("summery list",summeryList.toString());


        holder.cloth.setText(summeryList.get(position).getCloth_name());
        holder.qty.setText(summeryList.get(position).getTotal());
        holder.serv_ic.setText(summeryList.get(position).getShort_name());
        holder.price.setText(summeryList.get(position).getPrice()+"0/-");
        Glide.with(context).load(summeryList.get(position).getCatgoryicon())
                .thumbnail(0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.cat_ic);


        if (position==summeryList.size()-1){
            holder.dvdr_gr.setVisibility(View.GONE);

        }


    }

    @Override
    public int getItemCount() {
        return summeryList.size();
    }

    public class ListHolder extends RecyclerView.ViewHolder{

        TextView cloth,qty,price,serv_ic ;
        ImageView cat_ic ;
        RelativeLayout dvdr_gr;

        public ListHolder(View itemView) {
            super(itemView);
            cloth = (TextView) itemView.findViewById(R.id.cloth_ic);
            qty = (TextView) itemView.findViewById(R.id.qty_ic);
            price = (TextView) itemView.findViewById(R.id.price_ic);
            cat_ic = (ImageView) itemView.findViewById(R.id.catagory_icon);
            serv_ic = (TextView) itemView.findViewById(R.id.service_icon);
            dvdr_gr = (RelativeLayout) itemView.findViewById(R.id.dvdr_gr);
        }
    }
}
