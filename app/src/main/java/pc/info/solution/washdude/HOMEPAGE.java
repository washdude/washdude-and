package pc.info.solution.washdude;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

import pc.info.solution.washdude.FragmentsPkG.FragmentDrawer;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.UTILLS.DatabaseHandler;
import pc.info.solution.washdude.UTILLS.PrefManager;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

public class HOMEPAGE extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private FragmentDrawer drawerFragment;
    private RelativeLayout cardView,track_lay;
    public static RelativeLayout bottom_lay;
    private SharedPreferenceClass sharedPreferenceClass;
    private String userId;
    public static boolean albumStatus = false;
    public static Toolbar toolbar;
    private ImageView mDialog , HomeImage;
    RelativeLayout layout_;
    int [] home_ic = {R.drawable.wd_home_bg,R.drawable.wd_home_bg_2,R.drawable.wd_home_lady};
    DatabaseHandler db;
    PrefManager prefManager;
    int versionCode;
    String versionName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        versionCode = BuildConfig.VERSION_CODE;
        versionName = BuildConfig.VERSION_NAME;
        setContentView(R.layout.activity_homepage);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        cardView = (RelativeLayout) findViewById(R.id.palce_lay);
        track_lay = (RelativeLayout) findViewById(R.id.track_lay);
        bottom_lay = (RelativeLayout) findViewById(R.id.bottom_lay);
        layout_ = (RelativeLayout) findViewById(R.id.layout_);
        HomeImage = (ImageView) findViewById(R.id.home_img);
        setSupportActionBar(toolbar);

        layout_.getBackground().setAlpha(100);

        Random r = new Random();
        int bn = r.nextInt(home_ic.length);

        HomeImage.setImageResource(home_ic[bn]);




        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(HOMEPAGE.this, OrderService.class);
                startActivity(intent);
            }
        });

        track_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(HOMEPAGE.this,OrderListActivity.class);
                startActivity(intent);
            }
        });


        //new offer_listAsyntask().execute(Serverlinks.popuppromo);
        new version_listAsyntask().execute(Serverlinks.version);

    }
    private class offer_listAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(HOMEPAGE.this);
            progressDialog.setMessage("Offer");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                //jsonObject.accumulate("user_id", _userid);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString1(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog.isShowing())
                progressDialog.cancel();

            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {

                    JSONObject jObject = new JSONObject(result);
                    if (jObject != null) {
                        int status = jObject.getInt("status");
                        String img = jObject.getString("image");


                        if (status == 1) {



                            if (img.equals("")) {

                            } else {
                                final Dialog dialog = new Dialog(HOMEPAGE.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                                dialog.setContentView(R.layout.dialog_layout);
                                dialog.setCancelable(false);

                                ImageView cross = (ImageView) dialog.findViewById(R.id.cross);
                                mDialog = (ImageView)dialog.findViewById(R.id.your_image);


                                Glide.with(HOMEPAGE.this).load(img)
                                        .thumbnail(0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).into(mDialog);
                                mDialog.setClickable(false);



                                //finish the activity (dismiss the image dialog) if the user clicks
                                //anywhere on the image
                                cross.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        dialog.cancel();
                                    }
                                });
                                dialog.show();

                            }


                        }

                    } else {
                        Toast.makeText(HOMEPAGE.this, "NetWork Error", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(HOMEPAGE.this, "Check Network Connection !!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.e("String ", e.toString());
            }
        }
    }
    private static String convertInputStreamToString1(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }


    @Override
    public void onBackPressed() {


        super.onBackPressed();
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {

        switch (position) {

            case 1:
                Intent intent2 = new Intent(HOMEPAGE.this, Profile_page.class);
                startActivity(intent2);
                drawerFragment.closeDrawer();
                break;
            case 2:

               Intent intent = new Intent(HOMEPAGE.this,OrderListActivity.class);
                startActivity(intent);
                break;
            case 3:
                Intent intent3 = new Intent(HOMEPAGE.this, WalletPage.class);
                startActivity(intent3);
                drawerFragment.closeDrawer();
                break;
            case 4:
                Intent intent4 = new Intent(HOMEPAGE.this, Membership.class);
                startActivity(intent4);

                drawerFragment.closeDrawer();
                break;

            case 5:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=pc.info.solution.washdude"));
                startActivity(browserIntent);
                drawerFragment.closeDrawer();
                break;

            case 6:
                String shareBody = "https://play.google.com/store/apps/details?id=pc.info.solution.washdude";
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                //sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share Using"));

                drawerFragment.closeDrawer();
                break;
            case 7:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HOMEPAGE.this);
                // set title
                alertDialogBuilder.setTitle("Log Out!!");
                // set dialog message
                alertDialogBuilder
                        .setMessage("Do You Want To Log Out?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                db = new DatabaseHandler(getBaseContext());
                                prefManager = new PrefManager(HOMEPAGE.this,userId);

                                sharedPreferenceClass.clearData();
                                Intent intent = new Intent(HOMEPAGE.this, Pre_LOGIN.class);
                                startActivity(intent);
                                db.delete_data();
                                prefManager.clearData();
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                break;

            case 8:
                Intent intent8 = new Intent(HOMEPAGE.this, ReferEarn.class);
                startActivity(intent8);

                drawerFragment.closeDrawer();
                break;





        }


    }
    private class version_listAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progress.setVisibility(View.VISIBLE);
        }

//<<<<<<< Updated upstream
        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                //jsonObject.accumulate("user_id", _userid);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString2(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //progress.setVisibility(View.GONE);

            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {

                    JSONObject jObject = new JSONObject(result);
                    if (jObject != null) {
                        int status = jObject.getInt("status");
                        String versonName = jObject.getString("version");


                        if (status == 1) {

                            if (versonName.equals(versionName)) {

                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(HOMEPAGE.this);
                                builder.setTitle("UPDATE APP");
                                builder.setMessage("There is an update for this app is avilable in the playstore, get here");
                                String positiveText = "UPDATE";
                                builder.setPositiveButton(positiveText,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // positive button logic
                                                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=pc.info.solution.washdude"); // missing 'http://' will cause crashed
                                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                                startActivity(intent);

                                            }
                                        });


                                builder.setNegativeButton("LATER",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // negative button logic
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }

                        }

                    } else {
                        Toast.makeText(HOMEPAGE.this, "NetWork Error", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(HOMEPAGE.this, "Bad Network Connection !!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.e("String ", e.toString());
            }
        }
    }

    private static String convertInputStreamToString2(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

//=======
////    @Override
////    public boolean onCreateOptionsMenu(Menu menu) {
////        // Inflate the menu; this adds items to the action bar if it is present.
////        getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
////        return true;
////    }
//
////    @Override
////    public boolean onOptionsItemSelected(MenuItem item) {
////
////        /*switch (item.getItemId()) {
////
////            case R.id.action_settings:
////
////        }*/
////        int id = item.getItemId();
////        if (id == R.id.action_settings10) {
////            /*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HOMEPAGE.this);
////            // set title
////            alertDialogBuilder.setTitle("Log Out!!");
////            // set dialog message
////            alertDialogBuilder
////                    .setMessage("Do You Want To Log Out?")
////                    .setCancelable(false)
////                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
////                        public void onClick(DialogInterface dialog, int id) {
////                            // if this button is clicked, close
////                            // current activity
////                            //sharedPreferenceClass.setValue_boolean("USER", false);
////                            sharedPreferenceClass.clearData();
////                            Intent intent = new Intent(HOMEPAGE.this, LoginPage.class);
////                            startActivity(intent);
////                            finish();
////                        }
////                    })
////                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
////                        public void onClick(DialogInterface dialog, int id) {
////                            // if this button is clicked, just close
////                            // the dialog box and do nothing
////                            dialog.cancel();
////                        }
////                    });
////
////            // create alert dialog
////            AlertDialog alertDialog = alertDialogBuilder.create();
////
////            // show it
////            alertDialog.show();
////            return true;*/
////        }
////
////        /*switch (item.getItemId()){
////
////
////            case R.id.action_profile:
////                Intent intent2 = new Intent(HOMEPAGE.this,ProfilePage.class);
////                startActivity(intent2);
////                break;
////
////        }*/
////
////
////        return super.onOptionsItemSelected(item);
////    }
//>>>>>>> Stashed changes
}
