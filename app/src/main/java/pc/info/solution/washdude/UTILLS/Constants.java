package pc.info.solution.washdude.UTILLS;

/**
 * Created by harneet on 28/07/16.
 */
public class Constants {

    // Shared Preferences
    public static final String loginDetailSharedPreference="LoginDetails";
    public static final String isUserLoggedIn="USER";
    public static final String name="name";
    public static final String userId="userid";
    public static final String email="email";
    public static final String mobile="mobile";
    public static final String profile_photo="profile_photo";
    public static final String gcmId="gcm_id";
    public static final String mobileVerified="mobile_varified";
    public static final String  Who_Login="Who_Login";
    public static final String  ref_code="ref_code";
    public static final String  ref_by="ref_by";



    // GCM Sender Id
    public static final String googleGCMApiSenderId ="64548794449";





}
