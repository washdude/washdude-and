package pc.info.solution.washdude;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Confirmed extends AppCompatActivity {
    String orderId;
    TextView txtV1;
    RelativeLayout order_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmed);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ordered Successfully");

        orderId = getIntent().getStringExtra("jsonorderId");
        txtV1=(TextView)findViewById(R.id.order_text);
        txtV1.setText("Booking Id :"+orderId);
        order_layout=(RelativeLayout)findViewById(R.id.track_lay) ;

        order_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Confirmed.this, OrderDetails.class);
                i.putExtra("jsonorderId", orderId);
                startActivity(i);
                finish();
            }
        });


        
    }

}
