package pc.info.solution.washdude.UTILLS;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by abhis on 7/25/2016.
 */
public class AdressManger {

  String PREF_NAME = "ADRESS";
  String KEY_ADRESS = "ADRESS_VALUE";

  SharedPreferences pref ;
  SharedPreferences.Editor editor ;
  Context context;

  public AdressManger(Context context){

      this.context = context;
      pref = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);

  }

  public void storeAdress(ArrayList<Adress> adressList){

      editor = pref.edit();
      Gson gson = new Gson();

      Log.d("ADRESS", "ADRESS: " + gson.toJson(adressList));

      editor.putString(KEY_ADRESS, gson.toJson(adressList));

      editor.commit();

  }

 public ArrayList<Adress> getAdress(){


     List<Adress> albums = new ArrayList<Adress>();

     if(pref.contains(KEY_ADRESS)){
         String json = pref.getString(KEY_ADRESS,null);
         Gson gson = new Gson();
         Adress[] albumArry = gson.fromJson(json,Adress[].class);
         albums = Arrays.asList(albumArry);
         albums = new ArrayList<Adress>(albums);
         return (ArrayList<Adress>) albums;

     }else
         return null;

 }

}
