package pc.info.solution.washdude.FragmentsPkG;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pc.info.solution.washdude.AdapterPkg.OrderAdapter;
import pc.info.solution.washdude.AdapterPkg.ServiceAdapter;
import pc.info.solution.washdude.R;
import pc.info.solution.washdude.Service_detail;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderFragmnet extends Fragment {

    RecyclerView recyclerView;
    RecyclerView service_list;
    JSONArray childArr;
    OrderAdapter adapter;
    ServiceAdapter serv_adapter;
    ArrayList<String> cloth_names, cloth_urls, cloth_ids;
    String userID = "userID";
    String icon_c;
    int tab_pos;



    public static OrderFragmnet newInstance(JSONObject json1,String ic_url,int i) {
        OrderFragmnet f = new OrderFragmnet();
        Bundle args = new Bundle();
        args.putString("ic_url",ic_url);
        args.putInt("tab_pos",i);
        args.putString("JSON", json1.toString());
        f.setArguments(args);
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_fragmnet, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        String json1 = getArguments().getString("JSON");
        icon_c =  getArguments().getString("ic_url");
        tab_pos = getArguments().getInt("tab_pos");

        cloth_names = new ArrayList<>();
        cloth_urls = new ArrayList<>();
        cloth_ids = new ArrayList<>();


        try {
            JSONObject jsonObj = new JSONObject(json1);
            getClothData(jsonObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        recyclerView.addItemDecoration(new SpacesItemDecoration(15));

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Intent intent = new Intent(getActivity(), Service_detail.class);
                intent.putExtra("clothIDLists", cloth_ids);
                intent.putExtra("clothNAMELists", cloth_names);
                intent.putExtra("POSTION", position);
                intent.putExtra("USERID", userID);
                intent.putExtra("ICON_CAT",icon_c);
                intent.putExtra("CLOTH_URL",cloth_urls.get(position));
                intent.putExtra("tab_pos",tab_pos);

                startActivityForResult(intent,9);
                //      CreateServiceDialog(cloth_ids,cloth_names,position);


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==9 && resultCode == Activity.RESULT_OK){


            getActivity().recreate();
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {


            // Add top margin only for the first item to avoid double space between items
            if (parent.getChildPosition(view) == cloth_names.size() - 1)
                outRect.bottom = space;

        }
    }

    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


    public void getClothData(JSONObject jsonObj) {


        try {
            childArr = jsonObj.getJSONArray("clothes");

            for (int i = 0; i < childArr.length(); i++) {

                JSONObject json1 = childArr.getJSONObject(i);

                String cloth_id = json1.getString("id");
                Log.e("cloth_id :", cloth_id);
                cloth_ids.add(cloth_id);
                String clothname = json1.getString("cloth");
                Log.e("CLOTHNAME", clothname);
                cloth_names.add(clothname);
                String clothUrl = json1.getString("img");
                Log.e("CLOTHURL", clothUrl);
                cloth_urls.add(clothUrl);

            }


            adapter = new OrderAdapter(getActivity().getBaseContext(), cloth_names, cloth_urls, userID, cloth_ids);
            recyclerView.setAdapter(adapter);


        } catch (Exception e) {


        }


    }

}
