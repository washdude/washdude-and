package pc.info.solution.washdude;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import pc.info.solution.washdude.UTILLS.Constants;

public class MobileNumberActivity extends AppCompatActivity {

    RelativeLayout saveMobileNumberLayout;
    private EditText mobileNumberEditText;
    private static final int INPUT_MOBILE_NUMBER = 1;
    private String signInRequest;
    private static final String TAG = "MobileNumberActivity";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_number);
        saveMobileNumberLayout=(RelativeLayout)findViewById(R.id.saveMobileNumberLayout);
        mobileNumberEditText = (EditText) findViewById(R.id.mobileNumberEditText);

        Intent intent = getIntent();

        signInRequest = intent.getStringExtra("SignInJsonRequest");
        Log.d(TAG," signInRequest : "+signInRequest);
//        try {
//
//            signInRequest = new JSONObject(jsonString);
//
//
//        }catch (Exception e)
//        {
//            e.printStackTrace();
//        }


        saveMobileNumberLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mobileNumberEditText.getText()!=null) {
                    Intent i = new Intent();
                    i.putExtra(Constants.mobile,mobileNumberEditText.getText().toString() );
                    setResult(INPUT_MOBILE_NUMBER,i);
                    finish();
                }

            }
        });
    }

}
