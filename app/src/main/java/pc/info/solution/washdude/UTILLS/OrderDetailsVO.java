package pc.info.solution.washdude.UTILLS;

/**
 * Created by harneet on 16/07/16.
 */
public class OrderDetailsVO {

    private String cloth;
    private String quantity;
    private String service;
    private String unitPrice;
    private String shtName;
    private String parentImg;

    public String getShtName() {
        return shtName;
    }

    public void setShtName(String shtName) {
        this.shtName = shtName;
    }

    public String getParentImg() {
        return parentImg;
    }

    public void setParentImg(String parentImg) {
        this.parentImg = parentImg;
    }

    public String getCloth() {
        return cloth;
    }

    public void setCloth(String cloth) {
        this.cloth = cloth;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public OrderDetailsVO() {
    }

    public OrderDetailsVO(String cloth, String quantity, String service, String unitPrice) {
        this.cloth = cloth;
        this.quantity = quantity;
        this.service = service;
        this.unitPrice = unitPrice;
    }

    @Override
    public String toString() {
        return "OrderDetailsVO{" +
                "cloth='" + cloth + '\'' +
                ", quantity='" + quantity + '\'' +
                ", service='" + service + '\'' +
                ", unitPrice='" + unitPrice + '\'' +
                '}';
    }
}
