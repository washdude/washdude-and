/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pc.info.solution.washdude.gcm;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;


import pc.info.solution.washdude.HOMEPAGE;
import pc.info.solution.washdude.R;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;


/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    private Intent callIntent;
    private SharedPreferences shp;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    private SharedPreferences preferences;
    SharedPreferenceClass sharedPreferenceClass;
    public static final String TAG = "GCM CC";

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        sharedPreferenceClass=new SharedPreferenceClass(this);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                Log.d(TAG, "Send error: " + extras.toString());

            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {

                Log.d(TAG, "Deleted messages on server: " + extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                // Post notification of received message.
                Log.d(TAG, "Notification Extras : "+ extras.toString());
                String title = extras.getString("title", null);
                String message = extras.getString("description", null);
                String notify_type = extras.getString("type", null);

                String who_login = sharedPreferenceClass.getValue_string("Who_Login");
                boolean login = sharedPreferenceClass.getValue_boolean(who_login);
                shp = getSharedPreferences("AuthToken", MODE_PRIVATE);
                // After 5 seconds redirect to another intent
                if (who_login.equals("USER") && login == true) {

                    if (notify_type.equals("order")) {
                        String jsonorderId = extras.getString("order_id");
                        String jsonorderStatus = extras.getString("status");
                        callIntent = new Intent(this, HOMEPAGE.class);
                        callIntent.putExtra("jsonorderId", jsonorderId);
                        callIntent.putExtra("jsonorderStatus", jsonorderStatus);
                        sendNotification(title, message);

                    } else if (notify_type.equals("membership")) {
                        String planId = extras.getString("order_id");
                        String used = "0";
//                        String[] bgColors = getApplicationContext().getResources().getStringArray(R.array.color_array);
//                        String colorname = bgColors[0];
                        String userId=shp.getString("USER_UID", null);
                        callIntent = new Intent(this, HOMEPAGE.class);
                        callIntent.putExtra("planId", planId);
                        callIntent.putExtra("used", used);
                        callIntent.putExtra("userId", userId);
//                        callIntent.putExtra("colorname", colorname);
                        sendNotification(title, message);
                    }
                    else if(notify_type.equals("offer"))
                    {
                        String couponId=extras.getString("order_id");
                        callIntent = new Intent(this,HOMEPAGE.class);
                        callIntent.putExtra("CODE",couponId);
                        sendNotification(title,message);

                    }else {

                        callIntent = new Intent(this, HOMEPAGE.class);
                        sendNotification(title, message);

                    }

                    Log.i(TAG, "Received: " + extras.toString());
                }
                else
                {
                    Intent i = new Intent(getBaseContext(), HOMEPAGE.class);
                    startActivity(i);
                }

            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.push_notify_rev : R.drawable.push_notify_rev;
    }


    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String title, String msg) {
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, callIntent, PendingIntent.FLAG_CANCEL_CURRENT);



        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getBaseContext().getResources(), getNotificationIcon()))
                .setSmallIcon(R.drawable.push_notify)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setAutoCancel(true)
                .setContentText(msg);

//         Set the notification vibrate option
        if (preferences.getBoolean("notifications_new_message_vibrate", true)) {
            mBuilder.setVibrate(new long[]{1000, 1000, 1000});
        }
        // Set the notification ringtone
        if (preferences.getString("notifications_new_message_ringtone", null) != null) {
            mBuilder.setSound(Uri.parse(preferences.getString("notifications_new_message_ringtone", null)));
        } else {
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(alarmSound);
        }

        // Show only if the notification are enabled
        if (preferences.getBoolean("notifications_new_message", true)) {
            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }
    }
}
