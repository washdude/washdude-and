package pc.info.solution.washdude;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pc.info.solution.washdude.AdapterPkg.ServiceAdapter;
import pc.info.solution.washdude.UTILLS.PrefManager;
import pc.info.solution.washdude.webservices.Serverlinks;

public class Service_detail extends AppCompatActivity {

    ArrayList<String> cloth_ids, cloth_names;
    int positon;
    RecyclerView service_list;
    ServiceAdapter serv_adapter;
    String userID;
    CollapsingToolbarLayout collaps;
    public static TextView finalPrice;
    PrefManager shp;
    RelativeLayout next;
    RelativeLayout blur;
    String icon_c,icon_cloth;
    ArrayList<String> serviceList, priceList, WashidList,iconList,shtnameList,msgList;
    ArrayList<Integer> count;
    ImageView itemImage,price_tot;
    int pos_tab;
    CoordinatorLayout coordinatorLayout;

    @Override
    public void onBackPressed() {

        int cnt = 0;
        count = new ArrayList<>();

        try {
            for (int i = 0; i < WashidList.size(); i++) {


                int c = shp.GetPrimaryCount(cloth_ids.get(positon) + WashidList.get(i));
                cnt = cnt + c;
            }
            shp.StorePrimaryCount(cnt, cloth_ids.get(positon));
            Log.d("Total count", String.valueOf(cnt));

        } catch (Exception e) {

            e.printStackTrace();

        }


        OrderService.DownloadOrderList(Serverlinks.clothlist,getBaseContext(),pos_tab);


        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        shp = new PrefManager(this, userID);
        itemImage = (ImageView) findViewById(R.id.cloth_img);
        price_tot = (ImageView) findViewById(R.id.price_total);

        collaps = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        blur = (RelativeLayout) findViewById(R.id.blur_);
        service_list = (RecyclerView) findViewById(R.id.service_list);
        TextView heading = (TextView) findViewById(R.id.CLOTHNAME);
        TextView detail = (TextView) findViewById(R.id.detail);
        finalPrice = (TextView) findViewById(R.id.finalPrice);
        next = (RelativeLayout) findViewById(R.id.total_layout);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.serv_cord);



        blur.getBackground().setAlpha(70);

        cloth_ids = getIntent().getStringArrayListExtra("clothIDLists");
        cloth_names = getIntent().getStringArrayListExtra("clothNAMELists");
        positon = getIntent().getIntExtra("POSTION", -1);
        userID = getIntent().getStringExtra("USERID");
        icon_c = getIntent().getStringExtra("ICON_CAT");
        icon_cloth = getIntent().getStringExtra("CLOTH_URL");
        pos_tab = getIntent().getIntExtra("tab_pos",0);
                Uri myUri = Uri.parse(icon_cloth);
        Glide.with(Service_detail.this)
                .load(myUri)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .into(itemImage);

        Glide.with(Service_detail.this)
                .load(icon_c)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .into(price_tot);

        heading.setText(cloth_names.get(positon));

        getSupportActionBar().setTitle("");
        collaps.setTitle("");

        float p = shp.GetFinalPrice();
        setFinalPriceIntext(p);



        service_list.setLayoutManager(new LinearLayoutManager(this));
        service_list.addItemDecoration(new SpacesItemDecoration(20));
        getServiceListData(cloth_ids.get(positon), cloth_names.get(positon));

       next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Service_detail.this, ChooseSlot.class);
                startActivity(intent);
                finish();
            }
        });

    }


    public static void setFinalPriceIntext(Float p) {

        if (p > 0) {
           Service_detail.finalPrice.setText( String.valueOf(p) + "0");
         //   Service_detail.priceTot.setText( String.valueOf(p) + "0");

        } else {
            Service_detail.finalPrice.setText("");
           // Service_detail.priceTot.setText("");

        }

    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {


            if (parent.getChildPosition(view) == 0)
                outRect.top = space + space;
            // Add top margin only for the first item to avoid double space between items
            if (parent.getChildPosition(view) == cloth_names.size() - 1)
                outRect.bottom = space + space + space;

        }
    }

    private void getServiceListData(String s, String s1) {

        serviceList = new ArrayList<>();
        priceList = new ArrayList<>();
        WashidList = new ArrayList<>();
        iconList = new ArrayList<>();
        shtnameList = new ArrayList<>();
        msgList = new ArrayList<>();

        final String clothId = s;
        final String clothName = s1;
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Service Available");
        progressDialog.show();

        StringRequest jreq = new StringRequest(Request.Method.POST, Serverlinks.servicelist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("LIST OF SERVICE", response.toString());
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    JSONArray entry = jsonObj.getJSONArray("data");


                    for (int i = 0; i < entry.length(); i++) {
                        JSONObject json1 = entry.getJSONObject(i);

                        String icon_url = json1.getString("img");
                        iconList.add(icon_url);
                        String washnmae = json1.getString("wash");
                        serviceList.add(washnmae);
                        String washid = json1.getString("wash_id");
                        WashidList.add(washid);
                        String price = json1.getString("price");
                        priceList.add(price);
                        String short_name = json1.getString("short_name");
                        shtnameList.add(short_name);
                        String msg = json1.getString("msg");
                        msgList.add(msg);


                    }
                    serv_adapter = new ServiceAdapter(getBaseContext(), serviceList, WashidList, priceList,
                                                  clothId, clothName, userID, iconList,icon_c,shtnameList,msgList,coordinatorLayout);
                    service_list.setAdapter(serv_adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                Log.d("SERVCICE ERROR", error.toString());

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("parent_cloth", clothId);

                return params;
            }

        };


        MyApplication.getInstance().addToRequestQueue(jreq);

    }
}
