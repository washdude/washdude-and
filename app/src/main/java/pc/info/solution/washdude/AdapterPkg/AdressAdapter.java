package pc.info.solution.washdude.AdapterPkg;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import pc.info.solution.washdude.Add_address;
import pc.info.solution.washdude.Profile_page;
import pc.info.solution.washdude.R;
import pc.info.solution.washdude.UTILLS.Adress;
import pc.info.solution.washdude.UTILLS.ProfileAddress;
import pc.info.solution.washdude.webservices.Serverlinks;

/**
 * Created by abhis on 7/25/2016.
 */
public class AdressAdapter extends RecyclerView.Adapter<AdressAdapter.adHolder> {

    Context context;
    private List<ProfileAddress> profAddr;
    private SharedPreferences shp;

    public class adHolder extends RecyclerView.ViewHolder {

        public TextView add_heading, addr_desc, area_desc;
        ImageView imgdel, imgedit;

        public adHolder(View itemView) {
            super(itemView);

            add_heading = (TextView) itemView.findViewById(R.id.addrId);
            addr_desc = (TextView) itemView.findViewById(R.id.adress_desc);
            area_desc = (TextView) itemView.findViewById(R.id.area_desc);
            imgdel = (ImageView) itemView.findViewById(R.id.del_addr);
            imgedit = (ImageView) itemView.findViewById(R.id.edit_addr);


        }
    }


    public AdressAdapter(Context contex, List<ProfileAddress> profAddr) {

        this.context = contex;
        this.profAddr = profAddr;


    }

    @Override
    public adHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.profile_address_litem, parent, false);
        adHolder holder = new adHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(final adHolder holder, final int position) {

        final ProfileAddress address = profAddr.get(position);
        holder.add_heading.setText(address.getAdd_heading());
        holder.area_desc.setText(address.getCircle_name());
        holder.addr_desc.setText(address.getAddress() + "," + address.getPincode());

        holder.imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create_dialogue(position);
                Intent i = new Intent(context, Add_address.class);
                i.putExtra("address", address);
                i.setFlags(i.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });

        holder.imgdel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete Address");
                builder.setMessage("Do you want delete this address ?");
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        



                    }
                });

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        new DeleteAddress(address).execute(Serverlinks.deleteaddr);
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();






            }
        });

    }

    @Override
    public int getItemCount() {
        return profAddr.size();
    }

    private class DeleteAddress extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        private ProfileAddress address;

        DeleteAddress(ProfileAddress address) {
            this.address = address;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            /*
             * progressDialog = new ProgressDialog(Addlocationnew.this);
			 * progressDialog.setMessage("Loading In...");
			 * progressDialog.setIndeterminate(false); progressDialog.show();
			 */
            //progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {
            shp = context.getSharedPreferences("AuthToken",
                    Context.MODE_PRIVATE);
            //hm_userid = shp.getString("USER_UID", null);

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                //jsonObject.accumulate("user_id",hm_userid);
                jsonObject.accumulate("addr_id", address.getAddr_id());
                json = jsonObject.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {


            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {

                    JSONObject jsonObject = new JSONObject(result);
                    String status = jsonObject.optString("status");
                    if (status.equals("1")) {
                        Toast.makeText(context, "Address deleted successfully", Toast.LENGTH_LONG).show();
                        profAddr.remove(address);
                        AdressAdapter.this.notifyDataSetChanged();

                    } else {

                        Toast.makeText(context, "Try Again", Toast.LENGTH_LONG).show();
                    }
                }

            } catch (Exception e) {
                Log.e("String ", e.toString());
            }
            //progress.setVisibility(View.GONE);

            super.onPostExecute(result);
        }

    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }


}
