package pc.info.solution.washdude;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.UTILLS.InputValidation;
import pc.info.solution.washdude.UTILLS.TypefaceSpan;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;


public class LoginPage extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    EditText editUser,editPass;
    private String username,password,name, email, mobile,userId,ref_code;
    private int mobileVerified=0;
    CardView view;
    private FloatingActionButton googleLoginButton,facebookLoginButton;
    private static final String TAG="LoginPage";
    private String gcmId="";
    private GoogleCloudMessaging gcm;
    private RelativeLayout signInLayout;
    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;
    SharedPreferenceClass sharedPreferenceClass;
    private static final int INPUT_MOBILE_NUMBER = 1;
    private GoogleSignInResult result;
    public static CallbackManager callbackmanager;
    private AccessTokenTracker accessTokenTracker;
    private String accessToken;
    private Boolean isFacebookSignIn=false;
    private JSONObject facebookResult;
    private JSONObject signInRequest;
    RelativeLayout arrow;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        signInLayout=(RelativeLayout)findViewById(R.id.loginButton);
        FacebookSdk.sdkInitialize(getApplicationContext());
//        LoginManager.getInstance().logOut();


        editUser = (EditText) findViewById(R.id.edit_user);
        editPass = (EditText) findViewById(R.id.edit_pass);
        arrow = (RelativeLayout) findViewById(R.id.backbtn);
        view = (CardView) findViewById(R.id.view);
        SpannableString s1 = new SpannableString("phone or email");
        s1.setSpan(new TypefaceSpan(this, "urban.otf"), 0, s1.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SpannableString s2 = new SpannableString("password");
        s2.setSpan(new TypefaceSpan(this, "urban.otf"), 0, s2.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        editUser.setHint(s1);
        editPass.setHint(s2);

        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginPage.this,Pre_LOGIN.class);
                startActivity(i);


                finish();

            }
        });

        googleLoginButton = (FloatingActionButton)findViewById(R.id.gmail_fab1);
        facebookLoginButton = (FloatingActionButton)findViewById(R.id.facebook_fab1);
        facebookLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFacebookSignIn=true;
                LoginManager.getInstance().logInWithReadPermissions(LoginPage.this, Arrays.asList("email"));

            }
        });
        signInLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                username = editUser.getText().toString();
                password = editPass.getText().toString();
                if(!InputValidation.isEditTextHasvalue(editUser))
                {
                 editUser.setError("Enter email id or mobile number");
                }
                else if(!InputValidation.isEditTextHasvalue(editPass))
                {
                    editPass.setError("Enter password");
                }
                else {
                    signIn(Serverlinks.login);
                }

            }
        });

        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
            Log.d(TAG, "Play Services Ok.");
            if (gcmId.isEmpty()) {
                Log.d(TAG, "Find Register ID.");
                registerInBackground();
            }

        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(androidClientID)
                .requestEmail()
                .build();


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        googleLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFacebookSignIn=false;
                googleSignIn();
            }
        });


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(LoginPage.this,HOMEPAGE.class);
                startActivity(intent);
                finish();

            }
        });

        getFacebookUserInfo();


    }

    private void signIn(String url)
    {
        try {

            signInRequest = new JSONObject();
            signInRequest.accumulate("username", username);
            signInRequest.accumulate("password", password);
            signInRequest.accumulate("gcm_id",gcmId);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, signInRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {


                    try {
                        Log.e(TAG, "Signup Response: " + response.toString());

                        if (response != null) {
                            handleJsonResponse(response);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Error: " + error.getMessage());
                }
            });
            MyApplication.getInstance().addToRequestQueue(jsonObjectRequest);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void getFacebookUserInfo() {

        Log.d(TAG,"Inside getFacebookUserInfo");

        callbackmanager = CallbackManager.Factory.create();

        // Set permissions
        // LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_photos", "public_profile"));
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
                if(currentAccessToken != null)
                    accessToken = currentAccessToken.getToken();
            }
        };
        // If the access token is available already assign it.
//		accessToken = AccessToken.getCurrentAccessToken().getToken();

        LoginManager.getInstance().registerCallback(callbackmanager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {

                        System.out.println("Success" + loginResult.getAccessToken().getToken());
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject json, GraphResponse response) {
                                        if (response.getError() != null) {
                                            // handle error
                                            Log.d(TAG,"Facebook Login Error");
                                        } else {
                                            Log.d(TAG,"Facebook Login Success");
                                            try {

//                                                loginRequestVO =new LoginRequestVO();
                                                String jsonresult = String.valueOf(json);
                                                System.out.println("JSON Result" + jsonresult);
                                                facebookResult=json;
                                                handleFacebookSignInResult(json,null);


//                                                new AuthLoginAsyncTask().execute(ServerLinks.authLogin);

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, name, email"); // Parámetros que pedimos a facebook
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        //Log.d(TAG_CANCEL,"On cancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(LoginPage.this, error.toString(), Toast.LENGTH_SHORT).show();
                        //Log.d(TAG_ERROR,error.toString());
                    }
                });


    }

    private void handleJsonResponse(JSONObject response) {
        try {
            int status = response.getInt("status");
             if (status == 1) {

                JSONObject entry = response.getJSONObject("data");
                name = entry.getString(Constants.name);
                userId = entry.getString(Constants.userId);
                email = entry.getString(Constants.email);
                mobile = entry.getString(Constants.mobile);
                ref_code = entry.getString(Constants.ref_code);
                mobileVerified= entry.getInt(Constants.mobileVerified);
                saveUserDetails();
                if(mobileVerified==0) {
                    Intent intent = new Intent(LoginPage.this, OtpConfirmation.class);
                    intent.putExtra("SignInJsonRequest",signInRequest.toString());
                    startActivity(intent);
                    finish();
                }
                else{
                    Intent intent = new Intent(LoginPage.this, HOMEPAGE.class);
                    startActivity(intent);
                    finish();
                }


            }
             else if(status==400)
             {
                 Log.d(TAG,"Asking for mobile number");
                 showMobileNumberActivity();
             }
             else
            {
                String data = response.getString("data");
                Toast.makeText(getBaseContext(), data, Toast.LENGTH_SHORT).show();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showMobileNumberActivity()
    {
        Intent i =new Intent(LoginPage.this,MobileNumberActivity.class);
        startActivityForResult(i,INPUT_MOBILE_NUMBER);


    }

    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    private void saveUserDetails() {
        try {

            sharedPreferenceClass.setValue_string(Constants.userId, userId);
            sharedPreferenceClass.setValue_string(Constants.name, name);
            sharedPreferenceClass.setValue_string(Constants.mobile, mobile);
            sharedPreferenceClass.setValue_int(Constants.mobileVerified,mobileVerified);
            sharedPreferenceClass.setValue_boolean(Constants.isUserLoggedIn, true);
            sharedPreferenceClass.setValue_string(Constants.email, email);
            sharedPreferenceClass.setValue_string(Constants.ref_code, ref_code);
            sharedPreferenceClass.setValue_string(Constants.Who_Login, "USER");


        } catch (Exception e) {
            e.printStackTrace();
            finish();

        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, 9000).show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
        if (requestCode == RC_SIGN_IN) {
            result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleGoogleSignInResult(result,null);
        }
        else if(requestCode== INPUT_MOBILE_NUMBER)
        {
            String mobileNumber=data.getStringExtra(Constants.mobile);
            if(isFacebookSignIn)
                handleFacebookSignInResult(facebookResult,mobileNumber);
            else
                handleGoogleSignInResult(result,mobileNumber);
        }
        else {
            callbackmanager.onActivityResult(requestCode, resultCode, data);

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(LoginPage.this,Pre_LOGIN.class);
        startActivity(i);


        finish();
    }

    //After the signing we are calling this function
    private void handleGoogleSignInResult(GoogleSignInResult result,String mobile) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();

            try {
                signInRequest = new JSONObject();
                signInRequest.accumulate("name", acct.getDisplayName());
                signInRequest.accumulate("email", acct.getEmail());
                signInRequest.accumulate("google_id", acct.getId());
                signInRequest.accumulate("gcm_id", gcmId);
                signInRequest.accumulate("login_type",2);
                signInRequest.accumulate("mobile",mobile);
                Log.d(TAG,signInRequest.toString());
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Serverlinks.signUp, signInRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e(TAG, "Signup Response: " + response.toString());

                            if (response != null) {
                                handleJsonResponse(response);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Error: " + error.getMessage());
                    }
                });
                MyApplication.getInstance().addToRequestQueue(jsonObjectRequest);

            }catch (JSONException e)
            {e.printStackTrace();}


        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }


    //After the signing we are calling this function
    private void handleFacebookSignInResult(JSONObject result,String mobile) {
        //If the login succeed

            try {

                signInRequest = new JSONObject();
                signInRequest.accumulate("name", result.getString("name"));
                signInRequest.accumulate("email", result.getString("email"));
                signInRequest.accumulate("fb_id", result.getString("id"));
                signInRequest.accumulate("gcm_id", gcmId);
                signInRequest.accumulate("login_type",1);
                signInRequest.accumulate("mobile",mobile);
                Log.d(TAG,"Facebook Request : "+signInRequest.toString());
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Serverlinks.signUp, signInRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e(TAG, "Facebook Signin Response: " + response);

                            if (response != null) {
                                handleJsonResponse(response);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Error: " + error.getMessage());
                    }
                });
                MyApplication.getInstance().addToRequestQueue(jsonObjectRequest);

            }catch (JSONException e)
            {e.printStackTrace();}


    }



    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(LoginPage.this);
                    }
                    gcmId = gcm.register(Constants.googleGCMApiSenderId);
                    msg = "Device registered, registration ID=" + gcmId;
                    Log.d(TAG, "ID GCM: " + gcmId);
                    setGcmId(gcmId);

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);


    }

    public void setGcmId(String gcmId) {
        Log.e("gcmId ",""+gcmId);
        sharedPreferenceClass.setValue_string(Constants.gcmId, gcmId);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
