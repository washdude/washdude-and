package pc.info.solution.washdude.FragmentsPkG;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import pc.info.solution.washdude.R;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

/**
 * A simple {@link Fragment} subclass.
 */
public class PassWord_frag extends Fragment {
    EditText password,cnfpassword;
    CardView save;
    String new_pass,new_passn;
    private SharedPreferenceClass sharedPreferenceClass;
    private String userId;


    public PassWord_frag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pass_word, container, false);

        sharedPreferenceClass = new SharedPreferenceClass(getContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);

        password = (EditText) view.findViewById(R.id.edtPass);
        cnfpassword = (EditText) view.findViewById(R.id.edtPassN);
        save = (CardView) view.findViewById(R.id.view2);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new_pass = password.getText().toString();
                new_passn = cnfpassword.getText().toString();
                if (new_pass.equals(new_passn)) {

                    new User_password_HttpAsyncTask().execute(Serverlinks.passwordchange);

                } else {

                    Toast.makeText(getContext(), "Password don't match", Toast.LENGTH_LONG).show();

                }

            }
        });




        return view;
    }
    private class User_password_HttpAsyncTask extends AsyncTask<String, Void, String> {
        //ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           // progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", userId);
                jsonObject.accumulate("password", new_pass);
                /*deviceid1
						deviceid2
				senderreferral*/
                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }
            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String result) {
           // progressBar.setVisibility(View.GONE);
            try {
                if (!result.equals("")) {
                    JSONObject jObject = new JSONObject(result);
                    String status = jObject.optString("status");
                    if (status.equals("1")) {
                        Toast.makeText(getContext(), "Password changed Successfully", Toast.LENGTH_LONG).show();
//                        finish();
                    } else {
                        Toast.makeText(getContext(), "Failed", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Bad Network Connection !!",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            // progressDialog.dismiss();
            super.onPostExecute(result);
        }

    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

}
