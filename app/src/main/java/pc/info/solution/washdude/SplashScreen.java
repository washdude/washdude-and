package pc.info.solution.washdude;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

public class SplashScreen extends AppCompatActivity {
    private SharedPreferences sharedPreferences;
    SharedPreferenceClass sharedPreferenceClass;
    ImageView splash;
    RelativeLayout lay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferenceClass=new SharedPreferenceClass(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinate);
        splash = (ImageView) findViewById(R.id.splash);
        lay = (RelativeLayout) findViewById(R.id.lay);




        if (isConnected()){
            lay.getBackground().setAlpha(0);
            goToNext();
        }else {
            lay.getBackground().setAlpha(150);
            Snackbar snackbar = Snackbar.
                    make(coordinatorLayout,"Please check your Internet Connection",Snackbar.LENGTH_INDEFINITE)
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            recreate();
                        }
                    });
            snackbar.setActionTextColor(Color.parseColor("#ff5353"));
            snackbar.show();

        }




    }

    public void goToNext() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String who_login=sharedPreferenceClass.getValue_string("Who_Login");
                boolean login=sharedPreferenceClass.getValue_boolean(who_login);
                // After 5 seconds redirect to another intent
                if(who_login.equals("USER")&&login==true){
                    int mobileVerified=sharedPreferenceClass.getValue_int(Constants.mobileVerified);
                    if(mobileVerified==0) {
                        Intent intent4 = new Intent(SplashScreen.this,OtpConfirmation.class);
                        startActivity(intent4);
                        finish();
                    }else {
                        Intent i = new Intent(getBaseContext(), HOMEPAGE.class);
                        startActivity(i);
                        //overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        finish();
                    }
                }
                else{
                    Intent i = new Intent(getBaseContext(), Pre_LOGIN.class);
                    startActivity(i);
                    //overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    finish();
                }


            }
        }, 5000);


    }


    public  boolean isConnected() {
        ConnectivityManager
               /* cm = (ConnectivityManager)getBa
                .getSystemService(Context.CONNECTIVITY_SERVICE);*/
        cm = (ConnectivityManager) getBaseContext().getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

}
