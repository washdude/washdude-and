package pc.info.solution.washdude.FragmentsPkG;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pc.info.solution.washdude.AdapterPkg.AdressAdapter;
import pc.info.solution.washdude.Add_address;
import pc.info.solution.washdude.MyApplication;
import pc.info.solution.washdude.Profile_page;
import pc.info.solution.washdude.R;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.UTILLS.ProfileAddress;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

/**
 * A simple {@link Fragment} subclass.
 */
public class Adress_frag extends Fragment {


    static RecyclerView recyclerView;
    static AdressAdapter adapter;
    CardView address;
    static List<ProfileAddress> profAddr;
    private SharedPreferenceClass sharedPreferenceClass;
    private  String userId;
   static Context context_;
    public static String user_profile="http://139.59.12.252/washdude/Mapi/getUserProfile";





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_adress_frag, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.profle_ad_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        address = (CardView) view.findViewById(R.id.address);

        sharedPreferenceClass = new SharedPreferenceClass(getContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);


        setAdapter(getActivity(),userId);

        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), Add_address.class);

                   startActivity(intent);
                   getActivity().finish();
            }
        });




        return view;
    }

    public static void setAdapter(final Context context, final String user_id) {

        profAddr = new ArrayList<>();


      StringRequest jreq = new StringRequest(Request.Method.POST, Serverlinks.user_profile, new Response.Listener<String>() {
          @Override
          public void onResponse(String response) {

              Log.d("Adress list",response.toString());


              try {
                  JSONObject obj = new JSONObject(response);
                  String data = obj.getString("data");
                  JSONObject dtaObject = new JSONObject(data);
                  final JSONArray jArr = dtaObject.getJSONArray("address");
                  profAddr = new Gson().fromJson(jArr.toString(), new TypeToken<ArrayList<ProfileAddress>>() {
                  }.getType());


                  adapter = new AdressAdapter(context,profAddr);
                  recyclerView.setAdapter(adapter);


              } catch (Exception e) {
                  e.printStackTrace();
              }


          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {

          }
      })
      {
          @Override
          protected Map<String, String> getParams() {
              Map<String, String> params = new HashMap<String, String>();
              params.put("user_id", user_id);

              return params;
          }

      } ;

        MyApplication.getInstance().addToRequestQueue(jreq);



    }

}
