package pc.info.solution.washdude.AdapterPkg;

import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import pc.info.solution.washdude.ChooseSlot;
import pc.info.solution.washdude.R;

/**
 * Created by abhis on 7/30/2016.
 */
public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.slotHolder> {

    private static final String TAG="TimeSlotAdapter";
    Context context;
    ArrayList<String> addslotId;
    ArrayList<String> addslotDesc;
    public int mSelectedItem = -1;
    private TimeSlotSelectedListener timeSlotSelectedListener;

    public TimeSlotAdapter(Context context, ArrayList<String> addslotId, ArrayList<String> addslotDesc,TimeSlotSelectedListener timeSlotSelectedListener){

        this.context = context;
        this.addslotId=addslotId;
        this.addslotDesc=addslotDesc;
        this.timeSlotSelectedListener = timeSlotSelectedListener;

    }

    @Override
    public slotHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.slot_item_layout,parent,false);
        slotHolder holder = new slotHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(final slotHolder holder, final int position) {
        holder.sw.setChecked(position == mSelectedItem);

        holder.textView.setText(addslotDesc.get(position));


        holder.sw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mSelectedItem = holder.getAdapterPosition();
                notifyItemRangeChanged(0,addslotId.size());
                Log.d(TAG,"mSelectedItem : "+mSelectedItem);
                timeSlotSelectedListener.onTimeSlotSelected(mSelectedItem);

            }
        });





    }

    @Override
    public int getItemCount() {
        return addslotId.size();
    }

    public class slotHolder extends RecyclerView.ViewHolder{

        AppCompatRadioButton sw ;
        TextView textView;


        public slotHolder(View itemView) {
            super(itemView);

            sw = (AppCompatRadioButton) itemView.findViewById(R.id.sw_cmt);
            textView = (TextView) itemView.findViewById(R.id.time_);
        }
    }

    public interface TimeSlotSelectedListener{
        public void onTimeSlotSelected(int selectedIndex);
    }
}
