package pc.info.solution.washdude.UTILLS;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by abhis on 7/23/2016.
 */
public class PrefManager {

    SharedPreferences pref;

    SharedPreferences.Editor editor;

    Context context;

    String Pref_name = "Count";

    String totalPrice = "TOTAL_PRICE";


    public PrefManager(Context context, String PREF_NAME) {

        this.context = context;

        pref = context.getSharedPreferences(Pref_name, Context.MODE_PRIVATE);


    }


    public void Store_count(int count, String KEY_COUNT) {


        editor = pref.edit();
        editor.putInt(KEY_COUNT, count);
        Log.d("TAG", String.valueOf(count));
        editor.commit();


    }


    public int getCount(String KEY_COUNT) {


        int count = 0;

        if (pref.contains(KEY_COUNT)) {

            count = pref.getInt(KEY_COUNT, 0);
            return count;
        } else {

            return 0;
        }
    }


    public void storePrice(float price, String key_name) {

        editor = pref.edit();
        editor.putFloat(key_name, price);
        Log.d("TAG", String.valueOf(price));
        editor.commit();
    }

    public float getPrice(String key_name) {
        float value;

        if (pref.contains(key_name)) {

            value = pref.getFloat(key_name, 0);
            return value;
        } else {

            return 0;
        }


    }


    public void SaveFinalPrice(Float price, String status) {
        float p = pref.getFloat(totalPrice, 0);
        if (status.equals("1")) {

            p = p + price;
        } else {

            p = p - price;
        }

        if (p > 0) {
            editor = pref.edit();
            editor.putFloat(totalPrice, p);
            Log.d("TAG", String.valueOf(p));
            editor.commit();
        } else {
            p = 0;
            editor = pref.edit();
            editor.putFloat(totalPrice, p);
            Log.d("TAG", String.valueOf(p));
            editor.commit();

        }


    }


    public float GetFinalPrice() {

        float value;

        if (pref.contains(totalPrice)) {

            value = pref.getFloat(totalPrice, 0);
            return value;
        } else {

            return 0;
        }


    }


    public void StorePrimaryCount(int cnt, String KEY_PRIMARY_CNT) {

        editor = pref.edit();
        editor.putInt(KEY_PRIMARY_CNT, cnt);
        Log.d("PRIMARY COUNT", String.valueOf(cnt));
        editor.commit();


    }


    public int GetPrimaryCount(String KEY_PRIMARYT_CNT) {


        int count = 0;

        if (pref.contains(KEY_PRIMARYT_CNT)) {

            count = pref.getInt(KEY_PRIMARYT_CNT, 0);
            return count;
        } else {

            return 0;
        }
    }


    public void clearData(){

        editor = pref.edit();
        editor.clear().commit();
    }

}
