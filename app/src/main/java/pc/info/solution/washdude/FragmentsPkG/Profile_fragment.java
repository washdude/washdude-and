package pc.info.solution.washdude.FragmentsPkG;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import pc.info.solution.washdude.R;
import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;
/**
 * A simple {@link Fragment} subclass.
 */
public class Profile_fragment extends Fragment {
    String f_name, f_email, f_mbl, f_uid;
    EditText name, mobile, email;
    private SharedPreferenceClass sharedPreferenceClass;
    private String userId;
    static String emailid, new_phn, new_name;
    CardView save;
    String name_text,phone_text,email_text;


    public Profile_fragment(){


    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sharedPreferenceClass = new SharedPreferenceClass(getContext());
        userId = sharedPreferenceClass.getValue_string(Constants.userId);
        View view = inflater.inflate(R.layout.fragment_profile_fragment, container, false);
        name = (EditText) view.findViewById(R.id.name_edit);
        mobile = (EditText) view.findViewById(R.id.phone_edit);
        email = (EditText) view.findViewById(R.id.email_edit);
        save = (CardView) view.findViewById(R.id.view2);

        name_text = getArguments().getString("name");
        email_text = getArguments().getString("email");
        phone_text = getArguments().getString("mob");

        name.setText(name_text);
        email.setText(email_text);
        mobile.setText(phone_text);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new_name = name.getText().toString();
                new_phn = mobile.getText().toString();
                emailid = email.getText().toString();
                new Updateprofile().execute(Serverlinks.edit_profile);
            }
        });



        return view;
    }
    private class Updateprofile extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           // progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... urls) {
            InputStream inputStream = null;
            String result = "";

            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("user_id", userId);
                jsonObject.accumulate("email", emailid);
                jsonObject.accumulate("name", new_name);
                jsonObject.accumulate("mobile", new_phn);
                /*
                 * jsonObject.accumulate("mobile", Mobile);
				 * jsonObject.accumulate("password", Password);
				 */
                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                if (!result.equals("")) {
                    JSONObject jObject = new JSONObject(result);
                    int status = jObject.getInt("status");
                    if (status == 1) {
                        String mobile=new_phn;
                        sharedPreferenceClass.setValue_string(Constants.mobile, mobile);
                        FragmentDrawer.text_mob.setText(mobile);
                        Toast.makeText(getContext(), "Update Successfully", Toast.LENGTH_SHORT).show();


                    } else if (status == 2) {

                        Toast.makeText(getContext(), "Email Id  is already Registered ,Please Use any other emailid ", Toast.LENGTH_SHORT).show();
                    } else if (status == 3) {
                        Toast.makeText(getContext(), "Mobile number already in use, Please use any other email id", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Unable to store. Please try again.", Toast.LENGTH_SHORT).show();

                    }
                }/* else {
                    Intent i = new Intent(New_prfoile_sd.this, NointernetActivity.class);
                    startActivity(i);

                }*/
            } catch (Exception e) {

            }

            //progressBar.setVisibility(View.GONE);

            super.onPostExecute(result);
        }

    }
    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

}
