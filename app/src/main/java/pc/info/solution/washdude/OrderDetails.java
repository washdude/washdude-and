package pc.info.solution.washdude;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pc.info.solution.washdude.AdapterPkg.Orderdetails_adapter;
import pc.info.solution.washdude.AdapterPkg.SummeryListAdapter;
import pc.info.solution.washdude.UTILLS.DatabaseHandler;
import pc.info.solution.washdude.UTILLS.OrderDetailsVO;
import pc.info.solution.washdude.UTILLS.PrefManager;
import pc.info.solution.washdude.webservices.AllStaticVariables;
import pc.info.solution.washdude.webservices.AsynkTaskCommunicationInterface;
import pc.info.solution.washdude.webservices.AsynkTaskForServerCommunication;
import pc.info.solution.washdude.webservices.Serverlinks;

public class OrderDetails extends AppCompatActivity implements AsynkTaskCommunicationInterface {

    RecyclerView historyview;
    Orderdetails_adapter adapter;

    String orderId;
    RelativeLayout ord_pr,cnfm_pr,pick_pr,prgr_pr,delv_pr;
    LinearLayout gr1,gr2,gr3,gr4;

    TextView txtV1,txtV2,txtV3,txtV4,txtV5,txtV6,txtV7,txtV8,txtV9,txtV10,txtV11,txtV12;
    AsynkTaskForServerCommunication asynkTaskForServerCommunication;
    String json_orderdt, json_pickdt, json_delivdt, json_pickadd, json_deladdr,
            json_status, json_paidamnt, json_orderamnt, json_discamnt, json_couponcode, json_orderstatus,
            json_pickslot,json_delivslot,json_distype,json_clothcnt;
    int json_orderstatuscancel;
    double totPrice;
    String[] json_cloth, json_services, json_quantity, json_unitprice,json_parentimg,json_shtname;
    LinearLayout cpnLinear;
    Context activityContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        activityContext=this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        historyview = (RecyclerView) findViewById(R.id.history);
        historyview.setLayoutManager(new LinearLayoutManager(this));

        ord_pr = (RelativeLayout) findViewById(R.id.order_prgress);
        cnfm_pr = (RelativeLayout) findViewById(R.id.confirm_prgress);
        pick_pr = (RelativeLayout) findViewById(R.id.pickup_prgress);
        prgr_pr = (RelativeLayout) findViewById(R.id.process_prgress);
        delv_pr = (RelativeLayout) findViewById(R.id.deliv_prgress);

        gr1 = (LinearLayout) findViewById(R.id.grn_dvdr_1);
        gr2 = (LinearLayout) findViewById(R.id.grn_dvdr_2);
        gr3 = (LinearLayout) findViewById(R.id.grn_dvdr_3);
        gr4 = (LinearLayout) findViewById(R.id.grn_dvdr_4);



        txtV1 =(TextView) findViewById(R.id.order__id);
        txtV2 =(TextView) findViewById(R.id.pick_slot);
        txtV3 =(TextView) findViewById(R.id.deliv_slot);
        txtV4 =(TextView) findViewById(R.id.adress_slot);
        txtV5 =(TextView) findViewById(R.id.textView11);
        txtV6 =(TextView) findViewById(R.id.view3);
        txtV7 =(TextView) findViewById(R.id.tot_price_id);
        txtV8 =(TextView) findViewById(R.id.txtBillAmnt);
        txtV9 =(TextView) findViewById(R.id.cpn_value);
        txtV10 =(TextView) findViewById(R.id.cpnText);
        txtV11 =(TextView) findViewById(R.id.total_id);
        txtV12 =(TextView) findViewById(R.id.cancel);
        cpnLinear =(LinearLayout) findViewById(R.id.cpn_linear);


        orderId = getIntent().getStringExtra("jsonorderId");
        txtV1.setText("Order Id :"+orderId);


        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("order_id", orderId);

        HashMap<String, String> hashMapLink = new HashMap<String, String>();
        hashMapLink.put(AllStaticVariables.link, Serverlinks.order_dtls);
        asynkTaskForServerCommunication = new AsynkTaskForServerCommunication(OrderDetails.this);
        asynkTaskForServerCommunication.execute(parameters, hashMapLink);

    }
    @Override
    public void doInBackgroundForComun(int progress) {
        // TODO Auto-generated method stub

    }

    @Override
    public void doPostExecuteForCommu(JSONObject jsonObject) {
        // TODO Auto-generated method stub
        if (jsonObject != null) {
            JSONObject jsonResponse;
            //{"status":1}
            try {
                int status = jsonObject.getInt("status");
                String data = jsonObject.getString("data");
                Log.d("new_order",jsonObject.toString());
                Log.d("new_order",data);
                //Create a jsonObject of String data
                JSONObject dataObject = new JSONObject(data);
                if (status == 0) {
                    Toast.makeText(OrderDetails.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                } else if (status == 1) {
                    json_orderdt = dataObject.getString("order_date");
                    json_pickdt = dataObject.getString("pickup_date");
                    json_delivdt = dataObject.getString("delivery_date");
                    json_pickadd = dataObject.getString("pickupadd");

                    json_deladdr = dataObject.getString("deliveryadd");
                    json_status = dataObject.getString("status");
                    json_paidamnt = dataObject.getString("paid_amount");
                    json_orderamnt = dataObject.getString("order_price");
                    json_discamnt = dataObject.getString("discount_amount");
                    json_couponcode = dataObject.getString("coupon_code");

                    json_orderstatus = dataObject.getString("status");
                    json_orderstatuscancel = dataObject.getInt("order_status");
                    json_pickslot = dataObject.getString("pickup_slot");
                    json_delivslot = dataObject.getString("delivery_slot");
                    json_distype = dataObject.getString("dis_type");
                    json_clothcnt = dataObject.getString("total_clothes");

                    if(json_orderstatuscancel ==1){
                          ord_pr.setBackgroundResource(R.drawable.progrssbar_full);
                          gr1.setBackgroundColor(Color.parseColor("#0DCAA6"));
                    }else if(json_orderstatuscancel ==2 || json_orderstatuscancel ==3 || json_orderstatuscancel ==4 || json_orderstatuscancel ==6){
                        ord_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr1.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        cnfm_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr2.setBackgroundColor(Color.parseColor("#0DCAA6"));

                    }else if(json_orderstatuscancel ==5 || json_orderstatuscancel ==7 ){
                        ord_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr1.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        cnfm_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr2.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        pick_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr3.setBackgroundColor(Color.parseColor("#0DCAA6"));

                    }else if(json_orderstatuscancel ==8 || json_orderstatuscancel ==9 || json_orderstatuscancel ==10 || json_orderstatuscancel ==12){
                        ord_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr1.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        cnfm_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr2.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        pick_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr3.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        prgr_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr4.setBackgroundColor(Color.parseColor("#0DCAA6"));

                    }else if(json_orderstatuscancel ==11){
                        ord_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr1.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        cnfm_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr2.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        pick_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr3.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        prgr_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr4.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        delv_pr.setBackgroundResource(R.drawable.progrssbar_full);

                    }else if(json_orderstatuscancel ==13){
                        ord_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr1.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        cnfm_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr2.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        pick_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr3.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        prgr_pr.setBackgroundResource(R.drawable.progrssbar_full);
                        gr4.setBackgroundColor(Color.parseColor("#0DCAA6"));
                        delv_pr.setBackgroundResource(R.drawable.progrssbar_full);

                    }

                    if (json_orderstatuscancel == 1 || json_orderstatuscancel == 2 || json_orderstatuscancel == 3 || json_orderstatuscancel == 4 || json_orderstatuscancel == 6) {

                        txtV12.setVisibility(View.VISIBLE);

                    } else {
                        txtV12.setVisibility(View.INVISIBLE);
                    }

                    if(json_distype.equals("")){
                        cpnLinear.setVisibility(View.GONE);
                    }else{
                        cpnLinear.setVisibility(View.VISIBLE);
                        txtV10.setText(json_distype);
                    }

                    txtV2.setText(json_pickdt +", "+json_pickslot);
                    txtV3.setText(json_delivdt +", "+json_delivslot);
                    txtV4.setText(json_pickadd);
                    txtV11.setText(json_clothcnt);

                    getSupportActionBar().setTitle(json_orderstatus);

                    if (json_couponcode.equals("")) {
                        txtV5.setText("");
                    } else {
                        txtV5.setText("Cpn Code :" + json_couponcode);
                    }

                    totPrice = Double.parseDouble(json_orderamnt) -Double.parseDouble(json_paidamnt);
                    txtV7.setText(String.valueOf(json_orderamnt));
                    txtV8.setText(String.valueOf(json_paidamnt));
                    txtV9.setText(String.valueOf(totPrice));

                    if (json_paidamnt.equals("0")) {
                        txtV6.setText("Paid");
                    } else {
                        txtV6.setText("You Pay :" + json_paidamnt + "/-");
                    }

                    JSONArray jArr = dataObject.getJSONArray("clothdetails");
                    List<OrderDetailsVO> orderDetailsVOList =new ArrayList<>();
                    if (jArr != null) {
                        json_cloth = new String[jArr.length()];
                        json_services = new String[jArr.length()];
                        json_quantity = new String[jArr.length()];
                        json_unitprice = new String[jArr.length()];
                        json_parentimg = new String[jArr.length()];
                        json_shtname = new String[jArr.length()];
                        Log.d("new Order length",""+jArr.length());
                        for (int i = 0; i < jArr.length(); i++) {
                            jsonResponse = jArr.getJSONObject(i);

                            Log.d("json Response ",jsonResponse.toString());

                            json_cloth[i] = jsonResponse.getString("cloth");
                            json_services[i] = jsonResponse.getString("service");
                            json_quantity[i] = jsonResponse.getString("quantity");
                            json_unitprice[i] = jsonResponse.getString("unitprice");
                            json_parentimg[i] = jsonResponse.getString("parentiamge");
                            json_shtname[i] = jsonResponse.getString("short_name");


                            OrderDetailsVO tempOrderDetailsVO=new OrderDetailsVO();
                            tempOrderDetailsVO.setCloth(jsonResponse.getString("cloth"));
                            tempOrderDetailsVO.setService(jsonResponse.getString("service"));
                            tempOrderDetailsVO.setQuantity(jsonResponse.getString("quantity"));
                            tempOrderDetailsVO.setUnitPrice(jsonResponse.getString("unitprice"));
                            tempOrderDetailsVO.setParentImg(jsonResponse.getString("parentiamge"));
                            tempOrderDetailsVO.setShtName(jsonResponse.getString("short_name"));
                            orderDetailsVOList.add(tempOrderDetailsVO);
                        }
                    }
                    adapter = new Orderdetails_adapter(getBaseContext(),orderDetailsVOList);
                    historyview.setAdapter(adapter);

                    txtV12.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OrderDetails.this);
                            // set title
                            alertDialogBuilder.setTitle("Order Cancelled");
                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("Are You sure to cancel the order")
                                    .setCancelable(false)
                                    .setPositiveButton("Confirmed", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            /*Intent intent = new Intent(OrderDetails.this, LoginPage.class);
                                            startActivity(intent);*/

                                            new ordercancel_listAsyntask().execute(Serverlinks.order_cancel);



                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.cancel();
                                        }
                                    });

                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();


                        }
                    });

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private class ordercancel_listAsyntask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activityContext);
            progressDialog.show();
            //progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {

            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(urls[0]);

                String json = "";

                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("order_id", orderId);

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string
                // usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the
                // content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            // 11. return result
            return result;

            // return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog.isShowing())
                progressDialog.cancel();
            //progressBar.setVisibility(View.GONE);

            JSONObject jsonResponse;

            try {
                if (!result.equals("")) {

                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject != null) {
                        int status = jsonObject.getInt("status");
                        if (status == 0) {

                            Toast.makeText(OrderDetails.this, "Insufficient Data", Toast.LENGTH_SHORT).show();

                        } else if (status == 1) {


                            Toast.makeText(OrderDetails.this, "Your order has been cancelled", Toast.LENGTH_SHORT).show();

                        } else if (status == 2) {

                            Toast.makeText(OrderDetails.this, "Invalid Order Id", Toast.LENGTH_SHORT).show();

                        }
                    }

                }

            } catch (Exception e) {
                Log.e("String ", e.toString());
            }
            //progressBar.setVisibility(View.GONE);
            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

}
