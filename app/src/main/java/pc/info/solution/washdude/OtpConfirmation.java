package pc.info.solution.washdude;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import pc.info.solution.washdude.UTILLS.Constants;
import pc.info.solution.washdude.UTILLS.customtext;
import pc.info.solution.washdude.webservices.Serverlinks;
import pc.info.solution.washdude.webservices.SharedPreferenceClass;

public class OtpConfirmation extends AppCompatActivity {

    private String mobileNumber,userId,otp;
    customtext mobileTextView,otpTextView;
    TextView change,timerTextView;
    private EditText otpEditText;
    SharedPreferenceClass sharedPreferenceClass;
    private RelativeLayout resendOtp;
    private static final String TAG = "OtpConfirmation";
    private Boolean whetherSendOTPButton = false;
    private String signInRequest;
    private static final int INPUT_MOBILE_NUMBER = 1;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_confirmation);

//        Intent intent = getIntent();
//
//        signInRequest = intent.getStringExtra("SignInJsonRequest");
//        Log.d(TAG," signInRequest : "+signInRequest);

        sharedPreferenceClass = new SharedPreferenceClass(getApplicationContext());
        mobileTextView=(customtext)findViewById(R.id.mob);
        otpTextView=(customtext)findViewById(R.id.otpTextView);
        timerTextView=(TextView) findViewById(R.id.timerTextView);
        mobileNumber = sharedPreferenceClass.getValue_string(Constants.mobile);
        userId = sharedPreferenceClass.getValue_string(Constants.userId);
        otpEditText=(EditText)findViewById(R.id.otp_edit_text);

        resendOtp=(RelativeLayout)findViewById(R.id.resend_otp);
        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(whetherSendOTPButton)
                    sendOTP(Serverlinks.sendOTP);
                else
                    checkOTP(Serverlinks.checkOTP);
            }
        });
        mobileTextView.setText("Mobile No : "+mobileNumber);

        change = (TextView)findViewById(R.id.txtMobile);
        change.setVisibility(View.INVISIBLE);

//        change.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(OtpConfirmation.this,MobileNumberActivity.class);
//                intent.putExtra("SignInJsonRequest",signInRequest.toString());
//                startActivity(intent);
//            }
//        });
        if(userId!=null)
            sendOTP(Serverlinks.sendOTP);

    }
    @Override
    public void onBackPressed() {


        sharedPreferenceClass.setValue_int(Constants.mobileVerified,0);
        super.onBackPressed();


    }


    private String sendOTP(String url) {


        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("user_id", userId);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {


                    try {
                        Log.e(TAG, "SendOTP Response: " + response.toString());

                        if (response != null) {
                            handleSendOtpJsonResponse(response);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Error: " + error.getMessage());
                }
            });
            MyApplication.getInstance().addToRequestQueue(jsonObjectRequest);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


    private String checkOTP(String url) {


//        countDownTimer.cancel();
//        timerTextView.setVisibility(View.INVISIBLE);
        otp=otpEditText.getText().toString();

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("user_id", userId);
            jsonObject.accumulate("otp",otp);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {


                    try {
                        Log.e(TAG, "CheckOTP Response: " + response.toString());

                        if (response != null) {
                            handleCheckOTPJsonResponse(response);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Error: " + error.getMessage());
                }
            });
            MyApplication.getInstance().addToRequestQueue(jsonObjectRequest);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void handleCheckOTPJsonResponse(JSONObject response)
    {
        try {
            int status = response.getInt("status");
            if (status == 0) {

            } else if (status == 1) {

                String data= response.getString("data");
//                Toast.makeText(getBaseContext(), data, Toast.LENGTH_SHORT).show();
                sharedPreferenceClass.setValue_int(Constants.mobileVerified,1);
                finish();
                Intent i = new Intent(getBaseContext(), HOMEPAGE.class);
                startActivity(i);

            } else {
                String data = response.getString("data");
                Toast.makeText(getBaseContext(), data, Toast.LENGTH_SHORT).show();
//                otpTextView.setText("Resend OTP");
//                whetherSendOTPButton=true;

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleSendOtpJsonResponse(JSONObject response) {
        try {
            int status = response.getInt("status");
            if (status == 0) {

            } else if (status == 1) {

                String data= response.getString("data");
                Toast.makeText(getBaseContext(), data, Toast.LENGTH_SHORT).show();
                otpTextView.setText("Verify OTP");
                whetherSendOTPButton=false;
                timerTextView.setVisibility(View.VISIBLE);
                countDownTimer.start();
                sharedPreferenceClass.setValue_int(Constants.mobileVerified,1);

            }
            else if(status==400)
            {
                Log.d(TAG,"Asking for mobile number");
                showMobileNumberActivity();
            } else {
                String data = response.getString("data");
                Toast.makeText(getBaseContext(), data, Toast.LENGTH_SHORT).show();
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void showMobileNumberActivity()
    {
        Intent i =new Intent(OtpConfirmation.this,MobileNumberActivity.class);
        startActivityForResult(i,INPUT_MOBILE_NUMBER);
        finish();

    }
    CountDownTimer countDownTimer = new CountDownTimer(120000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {

            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);


            if (minutes > 0) {

                seconds = seconds - 60;

                timerTextView.setText("" + minutes + ":" + seconds);
            } else {

                timerTextView.setText("" + minutes + ":" + seconds);
            }


//            timerTextView.setText("" + millisUntilFinished / 1000);


            //here you can have your logic to set text to edittext
        }

        @Override
        public void onFinish() {

            timerTextView.setVisibility(View.GONE);
            otpTextView.setText("Resend OTP");
            whetherSendOTPButton = true;


        }
    };




}
