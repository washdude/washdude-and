package pc.info.solution.washdude.AdapterPkg;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import pc.info.solution.washdude.OrderService;
import pc.info.solution.washdude.R;
import pc.info.solution.washdude.Service_detail;
import pc.info.solution.washdude.UTILLS.Catagories;
import pc.info.solution.washdude.UTILLS.DatabaseHandler;
import pc.info.solution.washdude.UTILLS.PrefManager;

/**
 * Created by abhis on 7/20/2016.
 */
public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ListHolder> {
    Context context;
    ArrayList<String> serviceList, idList, priceList, iconList,shtnameList,msgList;
    String clothId, clothName, userID;
    DatabaseHandler db;
    PrefManager pref;
    int count;
    float price;
    String icon_c;
    CoordinatorLayout coordinatorLayout;


    public ServiceAdapter(Context baseContext, ArrayList<String> serviceList, ArrayList<String> idList,
                          ArrayList<String> priceList, String clothId,
                          String clothName, String userID, ArrayList<String> iconList, String icon_c, ArrayList<String> shtnameList, ArrayList<String> msgList,CoordinatorLayout coordinatorLayout ) {
        this.context = baseContext;
        this.serviceList = serviceList;
        this.priceList = priceList;
        this.idList = idList;
        this.iconList = iconList;
        this.clothId = clothId;
        this.clothName = clothName;
        this.userID = userID;
        this.icon_c = icon_c;
        this.shtnameList=shtnameList;
        this.msgList=msgList;
        this.coordinatorLayout = coordinatorLayout;

        db = new DatabaseHandler(context);
        pref = new PrefManager(context, userID);

    }


    @Override
    public ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.each_service_layout, parent, false);
        ListHolder holder = new ListHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(final ListHolder holder, final int position) {

        holder.serv_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snackbar = Snackbar.
                        make(coordinatorLayout,msgList.get(position),Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });

        count = pref.getCount(clothId + idList.get(position));
        price = pref.getPrice(clothId + idList.get(position) + position);

        Glide.with(context).load(iconList.get(position)).thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.icon_service);

        holder.servicename.setText(serviceList.get(position));
        holder.price.setText("Rs " + priceList.get(position) + ".00");

        if (count != 0 && price != 0) {
            holder.count.setVisibility(View.VISIBLE);
            holder.count.setText(String.valueOf(count));

        } else {


        }

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                count = pref.getCount(clothId + idList.get(position));
                price = pref.getPrice(clothId + idList.get(position) + position);
                count++;
                pref.Store_count(count, clothId + idList.get(position));
                price = price + Float.valueOf(priceList.get(position));
                pref.storePrice(price, clothId + idList.get(position) + position);

                pref.SaveFinalPrice(Float.valueOf(priceList.get(position)), "1");

                OrderService.setFinalPriceIntext(pref.GetFinalPrice());
                Service_detail.setFinalPriceIntext(pref.GetFinalPrice());



                Catagories catagories = new Catagories();
                catagories.setCloth_name(clothName);
                catagories.setWash_type(serviceList.get(position));
                catagories.setTotal(String.valueOf(count));
                catagories.setPrice(String.valueOf(price));
                catagories.setServiconUrl(iconList.get(position));
                catagories.setClothId(clothId);
                catagories.setWashId(idList.get(position));
                catagories.setCatgoryicon(icon_c);
                catagories.setShort_name(shtnameList.get(position));


                if (db.isExists(catagories)) {
                    db.updateCatagory(catagories);
                } else {
                    db.addCatagory(catagories);
                }

                holder.count.setVisibility(View.VISIBLE);
                holder.count.setText(String.valueOf(count));

            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count = pref.getCount(clothId + idList.get(position));
                price = pref.getPrice(clothId + idList.get(position) + position);
                count--;

                price = price - Float.valueOf(priceList.get(position));


                if (count > 0 && price > 0) {

                    pref.Store_count(count, clothId + idList.get(position));
                    pref.storePrice(price, clothId + idList.get(position) + position);
                    pref.SaveFinalPrice(Float.valueOf(priceList.get(position)), "0");

                    OrderService.setFinalPriceIntext(pref.GetFinalPrice());
                    Service_detail.setFinalPriceIntext(pref.GetFinalPrice());


                    holder.count.setVisibility(View.VISIBLE);
                    holder.count.setText(String.valueOf(count));


                    Catagories catagories = new Catagories();
                    catagories.setCloth_name(clothName);
                    catagories.setWash_type(serviceList.get(position));
                    catagories.setTotal(String.valueOf(count));
                    catagories.setPrice(String.valueOf(price));
                    catagories.setServiconUrl(iconList.get(position));
                    catagories.setClothId(clothId);
                    catagories.setWashId(idList.get(position));
                    catagories.setCatgoryicon(icon_c);
                    catagories.setShort_name(shtnameList.get(position));

                    if (db.isExists(catagories)) {

                        db.updateCatagory(catagories);


                    } else {

                        db.addCatagory(catagories);
                    }


                } else {
                    count = 0;
                    price = 0;
                    pref.Store_count(count, clothId + idList.get(position));
                    pref.storePrice(price, clothId + idList.get(position) + position);

                    holder.count.setVisibility(View.VISIBLE);
                    holder.count.setText(String.valueOf(count));

                    pref.SaveFinalPrice(Float.valueOf(priceList.get(position)), "0");

                    OrderService.setFinalPriceIntext(pref.GetFinalPrice());
                    Service_detail.setFinalPriceIntext(pref.GetFinalPrice());


                    Catagories catagories = new Catagories();
                    catagories.setCloth_name(clothName);
                    catagories.setWash_type(serviceList.get(position));
                    catagories.setTotal(String.valueOf(count));
                    catagories.setPrice(String.valueOf(price));
                    catagories.setServiconUrl(iconList.get(position));
                    catagories.setClothId(clothId);
                    catagories.setWashId(idList.get(position));
                    catagories.setCatgoryicon(icon_c);
                    catagories.setShort_name(shtnameList.get(position));


                    if (db.isExists(catagories)) {

                        db.updateCatagory(catagories);


                    } else {

                        db.addCatagory(catagories);
                    }

                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }


    public class ListHolder extends RecyclerView.ViewHolder {

        TextView servicename, price, count;
        RelativeLayout add, minus,serv_block;
        ImageView icon_service;



        public ListHolder(View itemView) {
            super(itemView);


            servicename = (TextView) itemView.findViewById(R.id.service_name);
            price = (TextView) itemView.findViewById(R.id.price_value);
            count = (TextView) itemView.findViewById(R.id.count);
            add = (RelativeLayout) itemView.findViewById(R.id.add);
            minus = (RelativeLayout) itemView.findViewById(R.id.minus);
            serv_block = (RelativeLayout) itemView.findViewById(R.id.serv_block);
            icon_service = (ImageView) itemView.findViewById(R.id.icon_service);

        }
    }
}
